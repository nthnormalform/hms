"use strict";

$(document).ready(function() {
    $.fn.datepicker.defaults.startDate = '2016-11-13';
    $.fn.datepicker.defaults.weekStart = 1;
    $.fn.datepicker.defaults.daysOfWeekHighlighted = '06';
    $.fn.datepicker.defaults.autoclose = true;
    $.fn.datepicker.defaults.format = "yyyy-mm-dd";

    $("input[name=destination]").focus(function () {
        $(this).parent().parent().removeClass('has-error');
    });
    $("#main_page_submit").click(function () {
        var current = $("input[name=destination]").val();

        for (var index = 0; index < destinations.length; ++index) {
            var d = destinations[index];
            if (d.name == current) {
                $("input[name=countryid]").val(d.countryId);
                $("input[name=stateid]").val(d.stateId);
                $("input[name=cityid]").val(d.cityId);
                return true;
            }
        }
        // If the entered string is not found
        $("input[name=destination]").parent().parent().addClass('has-error');
        return false;
    });

    $("#hotel-add-submit").click(function () {
        var current = $("#add-hotel-location").val();

        for (var index = 0; index < destinations.length; ++index) {
            var d = destinations[index];
            if (d.name == current) {
                $("#add-hotel-countryid").val(d.countryId);
                $("#add-hotel-stateid").val(d.stateId);
                $("#add-hotel-cityid").val(d.cityId);
                return true;
            }
        }
        // If the entered string is not found
        $("#add-hotel-location").parent().parent().addClass('has-error');
        return false;
    });

    $("#hotel-edit-submit").click(function () {
        var current = $("#edit-hotel-location").val();

        for (var index = 0; index < destinations.length; ++index) {
            var d = destinations[index];
            if (d.name == current) {
                $("#edit-hotel-countryid").val(d.countryId);
                $("#edit-hotel-stateid").val(d.stateId);
                $("#edit-hotel-cityid").val(d.cityId);
                return true;
            }
        }
        // If the entered string is not found
        $("#edit-hotel-location").parent().parent().addClass('has-error');
        return false;
    });

    $("#employees-table > tbody > tr").click(function(e) {
        $("#employees-table > tbody > tr").removeClass("node-selected");
        var object = e.target;
        if (object.nodeName == "TD" ||object.nodeName == "TH" )
            object = $(object.parentElement);
        else
            object = $(object);
        object.addClass("node-selected");
        var id = object.data("nodeid");
        if (id == undefined)
            alert("Undefined index! object: "+object);
        var name = object.children()[1].innerText;

        $("#edit-employee-id").val(id);
        $("#edit-employee-btn").removeAttr("disabled");
        $("#delete-employee-btn").removeAttr("disabled");
        $("#delete-employee-name").text(name);
        $("#delete-employee-id").val(id);
    });

    $("#edit-employee-dialog").on('show.bs.modal', function () {
        var sel = $('#employees-table > tbody > tr.node-selected');
        if (sel.length == 0)
            return;

        var id = $("#edit-employee-id").val();
        $("#edit-employee-firstname").val(decodeURIComponent(employees[id]['firstname']));
        $("#edit-employee-lastname").val(decodeURIComponent(employees[id]['lastname']));
        $("#edit-employee-middlename").val(decodeURIComponent(employees[id]['middlename']));
        $("#edit-employee-salary").val(employees[id]['salary']);
        $("#edit-employee-post").val(decodeURIComponent(employees[id]['post']));
    });

    $("#hotel-table > tbody > tr").click(function(e) {
        $("#hotel-table > tbody > tr").removeClass("node-selected");
        var object = e.target;
        if (object.nodeName == "TD" ||object.nodeName == "TH" )
            object = $(object.parentElement);
        else
            object = $(object);
        object.addClass("node-selected");
        var id = object.data("nodeid");
        if (id == undefined)
            alert("Undefined index! object: "+object);
        var name = object.children()[1].innerText;

        $("#edit-hotel-id").val(id);
        $("#edit-hotel-btn").removeAttr("disabled");
        $("#delete-hotel-btn").removeAttr("disabled");
        $("#delete-hotel-name").text(name);
        $("#delete-hotel-id").val(id);
    });

    $("#edit-hotel-dialog").on('show.bs.modal', function () {
        var sel = $('#hotel-table > tbody > tr.node-selected');
        if (sel.length == 0)
            return;

        var id = $("#edit-hotel-id").val();
        $("#edit-hotel-name").val(decodeURIComponent(hotels[id]['name']));
        $("#edit-hotel-location").val(decodeURIComponent(hotels[id]['cityName']) + ", " + decodeURIComponent(hotels[id]['stateName']) + ", " + decodeURIComponent(hotels[id]['countryName']));
        $("#edit-hotel-longitude").val(hotels[id]['longitude']);
        $("#edit-hotel-latitude").val(hotels[id]['latitude']);
        $("#edit-hotel-budget").val(hotels[id]['budget']);
        $("#edit-hotel-description").val(decodeURIComponent(hotels[id]['description']));
        $("#edit-hotel-countryid").val(hotels[id]['country']);
        $("#edit-hotel-stateid").val(hotels[id]['state']);
        $("#edit-hotel-cityid").val(hotels[id]['city']);
    });

    $("#roomtype-table > tbody > tr").click(function(e) {
        $("#roomtype-table > tbody > tr").removeClass("node-selected");
        var object = e.target;
        if (object.nodeName == "TD" ||object.nodeName == "TH" )
            object = $(object.parentElement);
        else
            object = $(object);
        object.addClass("node-selected");
        var id = object.data("nodeid");
        if (id == undefined)
            alert("Undefined index! object: "+object);
        var name = object.children()[1].innerText;

        $("#edit-roomtype-oldname").val(id);
        $("#edit-roomtype-btn").removeAttr("disabled");
        $("#delete-roomtype-btn").removeAttr("disabled");
        $("#delete-roomtype-name").text(name);
        $("#delete-roomtype-id").val(id);
    });

    $("#edit-roomtype-dialog").on('show.bs.modal', function () {
        var sel = $('#roomtype-table > tbody > tr.node-selected');
        if (sel.length == 0)
            return;

        var id = encodeURIComponent($("#edit-roomtype-oldname").val());
        $("#edit-roomtype-name").val(decodeURIComponent(roomtypes[id]['name']));
        $("#edit-roomtype-price").val(roomtypes[id]['price']);
        $("#edit-roomtype-description").val(decodeURIComponent(roomtypes[id]['description']));
    });

    $("#room-table > tbody > tr").click(function(e) {
        $("#room-table > tbody > tr").removeClass("node-selected");
        var object = e.target;
        if (object.nodeName == "TD" ||object.nodeName == "TH" )
            object = $(object.parentElement);
        else
            object = $(object);
        object.addClass("node-selected");
        var id = object.data("nodeid");
        if (id == undefined)
            alert("Undefined index! object: "+object);
        var name = object.children()[1].innerText;

        $("#edit-room-oldnumber").val(id);
        $("#edit-room-btn").removeAttr("disabled");
        $("#delete-room-btn").removeAttr("disabled");
        $("#delete-room-number").text(name);
        $("#delete-room-id").val(id);
    });

    $("#edit-room-dialog").on('show.bs.modal', function () {
        var sel = $('#room-table > tbody > tr.node-selected');
        if (sel.length == 0)
            return;

        var id = $("#edit-room-oldnumber").val();
        $("#edit-room-number").val(rooms[id]['number']);
        $("#edit-room-type").val(decodeURIComponent(rooms[id]['type']));
        $("#edit-room-status").val(rooms[id]['status']);
    });

    $("#button-change-password").click(function(e) {
        var object = e.target;
        if (object.nodeName == "BUTTON" )
            object = $(object.parentElement.parentElement);
        else alert("Error");

        var id = object.data("nodeid");
        if (id == undefined)
            alert("Undefined index! object: "+object);

        $("#edit-account-login").val(decodeURIComponent(id));
    });

    $(".button-checkin").click(function(e) {
        var object = e.target;
        if (object.nodeName == "BUTTON" )
            object = $(object.parentElement.parentElement);
        else alert("Error");

        var id = object.data("nodeid");
        if (id == undefined)
            alert("Undefined index! object: "+object);
        $("#checkin-roomnumber").val(id);
    });

    $(".button-checkin-nores").click(function(e) {
        var object = e.target;
        if (object.nodeName == "BUTTON" )
            object = $(object.parentElement.parentElement);
        else alert("Error");

        var id = object.data("nodeid");
        if (id == undefined)
            alert("Undefined index! object: "+object);

        $("#checkin-nores-roomnumber").val(id);
    });

    $(".button-checkout").click(function(e) {
        var object = e.target;
        if (object.nodeName == "BUTTON" )
            object = $(object.parentElement.parentElement);
        else alert("Error");

        var id = object.data("nodeid");
        if (id == undefined)
            alert("Undefined index! object: "+object);

        $("#checkout-roomnumber").val(id);
        $("#checkout-roomnumber-text").text(id);
    });

    $("#edit-employee-btn").attr("disabled", "disabled");
    $("#delete-employee-btn").attr("disabled", "disabled");
    $("#edit-hotel-btn").attr("disabled", "disabled");
    $("#delete-hotel-btn").attr("disabled", "disabled");
    $("#edit-roomtype-btn").attr("disabled", "disabled");
    $("#delete-roomtype-btn").attr("disabled", "disabled");
    $("#edit-room-btn").attr("disabled", "disabled");
    $("#delete-room-btn").attr("disabled", "disabled");
    $("#edit-reception-reservation-btn").attr("disabled", "disabled");
    $("#delete-reception-reservation-btn").attr("disabled", "disabled");
});