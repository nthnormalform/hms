<?php
namespace app\Model;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class BaseModel {
    /** @var  \MyApplication */
    protected $app;
    /** @var DatabaseWrapper */
    protected $db;
    /** @var \Monolog\Logger  */
    protected $log;

    /**
     * @param \MyApplication $app
     */
    function __construct(\MyApplication $app) {
        $this->app = $app;
        $this->db = $app['db_wrapper'];
        $this->log = $app['monolog'];
    }

    /**
     * Used to ensure that user has access to call some method.
     * Call this in the beginning of almost every method.
     *
     * @param $role
     */
    protected function checkRole($role) {
        if(!($this->app['security.authorization_checker']->isGranted('IS_AUTHENTICATED_FULLY')
            && $this->app['security.authorization_checker']->isGranted($role)))
            throw new AccessDeniedException();
    }

    protected function checkOneOfRoles($role1, $role2) {
        if(!($this->app['security.authorization_checker']->isGranted('IS_AUTHENTICATED_FULLY')
            && ($this->app['security.authorization_checker']->isGranted($role1)
            || $this->app['security.authorization_checker']->isGranted($role2))))
            throw new AccessDeniedException();
    }
}