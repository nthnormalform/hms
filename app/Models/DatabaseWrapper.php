<?php
namespace app\Model;

class DatabaseWrapper {

    /**
     * Debug stack format:
     * [queries] => Array ( [1] => Array (
     *      [sql] => SELECT 1+1;
     *      [params] => Array ( )
     *      [types] => Array ( )
     *      [executionMS] => 0.00017595291137695 ),
     *  [enabled] => 1
     *  [start] => 1477156602.4242
     *  [currentQuery] => 1);
     * @var \Doctrine\DBAL\Logging\DebugStack
     */
    private $dbDebugStack;
    /** @var  \MyApplication */
    protected $app;
    /** @var \Doctrine\DBAL\Connection */
    protected $dbal;
    /** @var \Monolog\Logger  */
    protected $log;

    /**
     * DatabaseWrapper constructor.
     * @param \MyApplication $app
     */
    function __construct(\MyApplication $app) {
        $this->app = $app;
        $this->log = $app['monolog'];

        // We need DebugStack to get the execution time of queries
        $this->dbal = $app['db'];
        $this->dbDebugStack = new \Doctrine\DBAL\Logging\DebugStack();
        $this->dbal->getConfiguration()->setSQLLogger($this->dbDebugStack);
    }

    /**
     * @return double
     */
    private function getLastSQLExecutionTime() {
        $lastQueryInfo = $this->dbDebugStack->queries[count($this->dbDebugStack->queries)];
        return $lastQueryInfo['executionMS'];
    }

    /**
     * Saves query statistics to the database.
     *
     * @param string $queryTemplate
     */
    private function saveQueryStatistics($queryTemplate) {
        $time = $this->getLastSQLExecutionTime();
        // TODO
    }

    /**
     * Retrieve assoc row of the first result row.
     *
     * For `SELECT * FROM user` it will return
     * array(
     *    'username' => 'jwage',
     *    'password' => 'changeme'
     *  )
     *
     * @param string $template
     * @param array $params
     * @param array $types
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function fetchAssoc($template, $params = array(), $types = array()) {
        $statement = $this->dbal->executeQuery($template, $params, $types);
        return $statement->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Executes the query and fetches all results into an array.
     *
     * For `SELECT * FROM user` it will return
     * array(
     *    0 => array(
     *      'username' => 'jwage',
     *      'password' => 'changeme'
     *      )
     *  )
     *
     * @param string $template
     * @param array $params
     * @param array $types
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function fetchAll($template, $params = array(), $types = array()) {
        $statement = $this->dbal->executeQuery($template, $params, $types);
        return $statement->fetchAll();
    }

    /**
     * @param string $template
     * @param array $params
     * @param array $types
     * @return int - number of affected records
     * @throws \Doctrine\DBAL\DBALException
     */
    public function insert($template, $params, $types = array()) {
        $count = $this->dbal->executeUpdate($template, $params, $types);
        return $count;
        // TODO: keep statistics
    }

    /**
     * @param string $template
     * @param array $params
     * @param array $types
     * @return int - number of affected records
     * @throws \Doctrine\DBAL\DBALException
     */
    public function update($template, $params, $types = array()) {
        $count = $this->dbal->executeUpdate($template, $params, $types);
        return $count;
        // TODO: keep statistics
    }

    /**
     * @param string $template
     * @param array $params
     * @param array $types
     * @return int - number of affected records
     * @throws \Doctrine\DBAL\DBALException
     */
    public function delete($template, $params, $types = array()) {
        $count = $this->dbal->executeUpdate($template, $params, $types);
        return $count;
        // TODO: keep statistics
    }

    /**
     * Returns the last inserted id.
     *
     * @return string
     */
    public function lastInsertId() {
        return $this->dbal->lastInsertId();
    }

    /**
     * Starts a new transaction.
     */
    public function beginTransaction() {
        $this->dbal->beginTransaction();
    }

    /**
     * Finishes the transaction.
     */
    public function commit() {
        $this->dbal->commit();
    }

    /**
     * Cancels the whole transaction.
     */
    public function rollback()  {
        $this->dbal->rollBack();
    }
}