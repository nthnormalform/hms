<?php
namespace app\Model;

use Symfony\Component\Security\Core\User\UserInterface;

class UserEntity implements UserInterface {
    private $login;
    private $password;
    private $roles;
    private $firstName;
    private $lastName;
    private $middleName;

    public function __construct($login, $password, array $roles, $firstName = null, $lastName = null, $middleName = null) {
        if ('' === $login || null === $login) {
            throw new \Exception('The username cannot be empty.');
        }

        $this->login = $login;
        $this->password = $password;
        $this->roles = $roles;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->middleName = $middleName;
    }

    public function getRoles() {
        return $this->roles;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getSalt() {
    }

    public function getUsername() {
        return $this->login;
    }

    public function eraseCredentials() {
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->login;
    }
}