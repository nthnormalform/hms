<?php
namespace app\Model;

class AdminModel extends BaseModel {
    function getStateAndCountryByCity($city) {
        $this->checkRole('ROLE_ADMIN');

        $template = 'SELECT State.id as stateId, State.countryID as countryId
                    FROM City, State
                    WHERE City.id = ? AND City.stateID = State.id
                    LIMIT 1;';

        return $this->db->fetchAssoc($template, array($city), array(\PDO::PARAM_INT));
    }

    function beginTransaction() {
        $this->db->beginTransaction();
    }

    function commit() {
        $this->db->commit();
    }

    function rollback() {
        $this->db->rollback();
    }
}