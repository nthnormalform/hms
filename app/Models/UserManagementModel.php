<?php
namespace app\Model;

class UserManagementModel extends BaseModel {

    function addUser($username, $rawPassword, $roles, $firstName = null, $lastName = null, $middleName = null) {
        $username = strtolower($username);
        if (self::doesUserNameExist($username))
            throw new \Exception("User already exists!");
        $passwordHash = null;
        if (!is_null($rawPassword))
            $passwordHash = @$this->app['security.default_encoder']->encodePassword($rawPassword);

        $this->db->insert("INSERT INTO WebSiteUser VALUES (?, ?, ?, ?, ?, ?);",
            array($username, $passwordHash, $roles, $firstName, $lastName, $middleName),
            array(\PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR));
    }

    private function doesUserNameExist($name) {
        $name = strtolower($name);
        return $this->db->fetchAssoc("SELECT COUNT(*) FROM WebSiteUser WHERE login = ?;",
            array($name),
            array(\PDO::PARAM_STR))["COUNT(*)"] > 0;
    }

    function changePassword($username, $newRawPassword) {
        $this->checkRole('ROLE_PASSWORD_CHANGE');
        $username = strtolower($username);
        if (!self::doesUserNameExist($username))
            throw new \Exception("User does not exist!");

        if (is_null($newRawPassword) || $newRawPassword == '')
            throw new \Exception("Password could not be empty!");

        $passwordHash = @$this->app['security.default_encoder']->encodePassword($newRawPassword);

        $this->db->update("UPDATE WebSiteUser SET password = ? WHERE login = ?",
            array($passwordHash, $username),
            array(\PDO::PARAM_STR, \PDO::PARAM_STR));
    }
}