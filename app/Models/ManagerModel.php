<?php
namespace app\Model;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ManagerModel extends BaseModel {
    /**
     * Returns the full information about all rooms.
     *
     * @param $hotelId
     * @return array
     */
    function getAllRoomsFullInfo($hotelId) {
        $this->checkRole('ROLE_FULL_ROOM_INFO_VIEWER');
        $sql = 'SELECT
            e.roomNumber       AS roomNumber,
            e.hotelId          AS hotelId,
            e.roomTypeName     AS roomTypeName,
            e.status           AS status,
            s.firstName           AS firstName,
            s.lastName            AS lastName,
            s.middleName          AS middleName,
            r.id                  AS reservationID,
            r.isPaid              AS isPaid,
            r.isCanceled          AS isCanceled,
            r.checkInDate         AS checkInDate,
            r.checkOutDate        AS checkOutDate,
            r.reserverEmail       AS reserverEmail,
            r.reserverPhoneNumber AS reserverPhoneNumber
        FROM (
            SELECT * FROM Room WHERE Room.hotelID = ?) as e
            LEFT JOIN (
                Stay AS s INNER JOIN (
                    SELECT *  FROM Reservation 
                    WHERE Reservation.hotelID = ? AND Reservation.isCanceled = FALSE
                        AND Reservation.checkInDate <= CURDATE() 
                        AND Reservation.checkOutDate >= CURDATE()) AS r 
            ON s.reservationID = r.id) 
            USING (roomNumber)
        WHERE r.hotelID IS NULL OR e.hotelId = r.hotelId;';
        return $this->db->fetchAll($sql, array($hotelId, $hotelId), array(\PDO::PARAM_INT, \PDO::PARAM_INT));
    }

    /**
     * Creates a new stay for existing reservation
     * and changes the room status using trigger.
     *
     * @param $reservationId
     * @param $roomNumber
     * @param $firstName
     * @param $lastName
     * @param $middleName
     * @throws \Exception
     */
    function checkIn($reservationId, $roomNumber, $firstName, $lastName, $middleName) {
        $this->checkRole('ROLE_RECEPTIONIST_SUBTYPE');
        $this->db->beginTransaction();
        try {
            $sql = "INSERT INTO Stay(reservationID, roomNumber, firstName, lastName, middleName) VALUES (?, ?, ?, ?, ?);";
            $this->db->insert($sql, array($reservationId, $roomNumber, $firstName, $lastName, $middleName),
                array(\PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR));

            // Update 'actualAmount'
            $sql = "UPDATE RoomReservation SET actualAmount = actualAmount + 1 
                    WHERE reservationID = ? 
                        AND roomTypeName = (SELECT room.roomTypeName
                                            FROM Reservation r, Room room
                                            WHERE r.id = ? 
                                                AND room.hotelID = r.hotelID
                                                AND room.roomNumber = ?);";
            $this->db->update($sql,
                array($reservationId, $reservationId, $roomNumber),
                array(\PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT));
            $this->db->commit();
        }
        catch(\Exception $e) {
            $this->db->rollback();
            throw $e;
        }
    }


    /**
     * Creates a new stay AND a new reservation for it.
     * Changes the room status using trigger.
     *
     * @param $hotelId
     * @param $checkInDate
     * @param $checkOutDate
     * @param $email
     * @param $phone
     * @param $isPaid
     * @param $roomNumber
     * @param $firstName
     * @param $lastName
     * @param $middleName
     * @throws \Exception
     */
    function checkInWithoutReservation($hotelId, $checkInDate, $checkOutDate, $email, $phone, $isPaid, $roomNumber, $firstName, $lastName, $middleName) {
        $this->checkRole('ROLE_RECEPTIONIST');
        $roomTypeName = $this->getRoom($hotelId, $roomNumber)['roomTypeName'];
        $rooms = array(array($roomTypeName, 1));
        $this->db->beginTransaction();

        try {
            $reservationId = $this->app['search_model']->addReservation($checkInDate, $checkOutDate, $hotelId, $email, $phone, $isPaid, $rooms);
            $this->checkIn($reservationId, $roomNumber, $firstName, $lastName, $middleName);
            $this->db->commit();
        }
        catch(\Exception $e) {
            $this->db->rollback();
            throw $e;
        }
    }

    function getRoom($hotelID, $roomNumber) {
        if(!($this->app['security.authorization_checker']->isGranted('IS_AUTHENTICATED_FULLY')
            && ($this->app['security.authorization_checker']->isGranted('ROLE_MANAGER') ||
                $this->app['security.authorization_checker']->isGranted('ROLE_RECEPTIONIST'))))
            throw new AccessDeniedException();

        $template = 'SELECT * FROM Room WHERE hotelID = ? AND roomNumber = ?;';
        return $this->db->fetchAssoc($template, array($hotelID, $roomNumber), array(\PDO::PARAM_INT, \PDO::PARAM_INT));
   }

    /**
     * Makes the room dirty.
     *
     * @param $hotelId
     * @param $roomNumber
     */
    function checkOut($hotelId, $roomNumber) {
        $this->checkRole('ROLE_RECEPTIONIST');
        $this->db->insert("UPDATE Room SET status = 2 WHERE roomNumber = ? AND hotelId = ?",
            array($roomNumber, $hotelId),
            array(\PDO::PARAM_INT, \PDO::PARAM_INT));
    }

    /**
     * Reservations for today that are available to check in
     *
     * @param $hotelId
     * @return array
     */
    function getReservationsAvailableForCheckIn($hotelId) {
        $this->checkRole('ROLE_RECEPTIONIST');
        $template = 'SELECT *    
                    FROM ReservationView 
                    WHERE hotelId = ? AND actualAmount < amount AND checkInDate = CURDATE();';
        return $this->db->fetchAll($template, array($hotelId), array(\PDO::PARAM_INT));
    }

    function getMyHotelId() {
        $this->checkRole('ROLE_RECEPTIONIST');

        $login = null;
        $token = $this->app['security.token_storage']->getToken();
        if (null !== $token) {
            $login = strtolower($token->getUser()->getUsername());
        }

        $template = 'SELECT id FROM Hotel WHERE receptionistLogin = ?;';
        return $this->db->fetchAssoc($template, array($login), array(\PDO::PARAM_STR))['id'];
    }

    /**
     * @param $id
     * @param $reserverEmail
     * @param $reserverPhoneNumber
     * @param $isPaid
     * @param array $rooms , $rooms[i] => array(roomTypeName, amount)
     * @return mixed
     * @throws \Exception
     */
    function updateReservation($id, $reserverEmail, $reserverPhoneNumber, $isPaid, array $rooms) {
        $this->checkRole('ROLE_RECEPTIONIST');
        $this->db->beginTransaction();
        try {
            $template = 'UPDATE Reservation 
                    SET reserverEmail = ?,
                    reserverPhoneNumber = ?,isPaid = ?
                    WHERE id = ?;';
            $this->db->update($template,
                array($reserverEmail, $reserverPhoneNumber, $isPaid, $id),
                array(\PDO::PARAM_STR,
                    \PDO::PARAM_STR, \PDO::PARAM_BOOL, \PDO::PARAM_INT));

            $template = 'DELETE 
                    FROM RoomReservation 
                    WHERE reservationID = ?;';
            $this->db->delete($template, array($id), array(\PDO::PARAM_INT));


            foreach ($rooms as $room) {
                $this->app['search_model']->addRoomReservation($id, $room[0], $room[1]);
            }
            $this->db->commit();
        }
        catch (\Exception $e) {
            $this->db->rollback();
            throw $e;
        }
        return $id;
    }

    function addEmployee($hotelID, $firstName, $lastName, $middleName, $position, $salary) {
        $this->checkRole('ROLE_MANAGER_SUBTYPE');
        $template = 'INSERT INTO Employee
                    (hotelID, firstName, lastName, middleName, position, salary)
                    VALUES 
                    (?, ?, ?, ?, ?, ?);';
        return $this->db->insert($template, array($hotelID, $firstName, $lastName, $middleName, $position, $salary),
            array(\PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_INT));
    }

    function editEmployee($employeeID, $firstName, $lastName, $middleName, $position, $salary) {
        $this->checkRole('ROLE_MANAGER');
        $template = 'UPDATE Employee
                    SET firstName = ?, lastName = ?, middleName = ?, position = ?, salary = ?
                    WHERE id = ?;';
        return $this->db->update($template, array($firstName, $lastName, $middleName, $position, $salary, $employeeID),
            array(\PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_INT, \PDO::PARAM_INT));
    }

    function getEmployees($hotelID) {
        $this->checkRole('ROLE_MANAGER');
        $template = 'SELECT *
                    FROM Employee 
                    WHERE hotelID = ?;';
        return $this->db->fetchAll($template, array($hotelID), array(\PDO::PARAM_INT));
    }

    function removeEmployee($employeeID) {
        $this->checkRole('ROLE_MANAGER');
        $template = 'DELETE 
                    FROM Employee 
                    WHERE id = ?;';
        return $this->db->delete($template, array($employeeID), array(\PDO::PARAM_INT));
    }

    function getRoomStatuses($hotelID) {
        if(!($this->app['security.authorization_checker']->isGranted('IS_AUTHENTICATED_FULLY')
            && ($this->app['security.authorization_checker']->isGranted('ROLE_MANAGER') ||
                $this->app['security.authorization_checker']->isGranted('ROLE_RECEPTIONIST'))))
            throw new AccessDeniedException();

        $template = 'SELECT status, COUNT(*) AS count
                    FROM Room
                    WHERE hotelID = ?
                    GROUP BY status;';

        return $this->db->fetchAll($template, array($hotelID), array(\PDO::PARAM_INT));
    }

    function getManagedHotels() {
        $this->checkRole('ROLE_MANAGER');

        $login = null;
        $token = $this->app['security.token_storage']->getToken();
        if (null !== $token) {
            $login = strtolower($token->getUser()->getUsername());
        }

        $template = 'SELECT h.id AS id, h.name AS name, h.country AS country, h.city AS city, h.state AS state, 
                    h.longitude AS longitude, h.latitude AS latitude, h.budget AS budget, h.description AS description,
                    h.receptionistLogin AS receptionistLogin, ct.name AS cityName, s.name AS stateName, c.name AS countryName
                    FROM Hotel h, Country c, State s, City ct
                    WHERE managerLogin = ? AND h.city = ct.id AND ct.stateID = s.id AND s.countryID = c.id;';
        return $this->db->fetchAll($template, array($login), array(\PDO::PARAM_STR));
    }

    function addHotel($name, $country, $state, $city, $longitude, $latitude, $budget, $description) {
        $this->checkRole('ROLE_MANAGER_SUBTYPE');
        $this->db->beginTransaction();

        $login = null;
        $token = $this->app['security.token_storage']->getToken();
        if (null !== $token) {
            $login = strtolower($token->getUser()->getUsername());
        }

        try {
            $template = 'INSERT INTO Hotel
                    (name, country, state, city, longitude, latitude, budget, description, picture, managerLogin)
                    VALUES 
                    (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);';
            $pictureUrl = null;
            // Hotel will have a picture with probability 8/10, otherwise add random picture
            if (rand(1, 10) < 9)
                $pictureUrl = '/img/hotels/'.rand(1, 441).'.jpg'; // we have 441 pictures there

            $this->db->insert($template,
                array($name, $country, $state, $city, $longitude, $latitude, $budget, $description, $pictureUrl, $login),
                array(\PDO::PARAM_STR, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT,
                    \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR));
            $hotelID = $this->db->lastInsertId();
            $this->app['user_management']->addUser('receptionist' . $hotelID, NULL, 'ROLE_RECEPTIONIST');
            $this->addReceptionist($hotelID, 'receptionist' . $hotelID);
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollback();
            throw $e;
        }
        return $hotelID;
    }

    private function addReceptionist($hotelID, $receptionistLogin) {
        $this->checkRole('ROLE_MANAGER_SUBTYPE');

        $template = 'UPDATE Hotel
                    SET receptionistLogin = ?
                    WHERE id = ?;';
        return $this->db->insert($template, array($receptionistLogin, $hotelID), array(\PDO::PARAM_STR, \PDO::PARAM_INT));
    }

    function editHotel($id, $name, $country, $state, $city, $longitude, $latitude, $budget, $description) {
        $this->checkRole('ROLE_MANAGER');
        $template = 'UPDATE Hotel
                    SET
                    name = ?, country = ?, state = ?, city = ?, longitude = ?, latitude = ?, budget = ?, description = ?
                    WHERE id = ?;';
        return $this->db->insert($template,
            array($name, $country, $state, $city, $longitude, $latitude, $budget, $description, $id),
            array(\PDO::PARAM_STR, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT,
                \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_INT));
    }

    function removeHotel($hotelID) {
        $this->checkRole('ROLE_MANAGER');
        $template = 'DELETE 
                    FROM Hotel
                    WHERE id = ?;';
        return $this->db->delete($template, array($hotelID), array(\PDO::PARAM_INT));
    }

    /**
     * @param $hotelID
     * @param array $roomTypes , $roomTypes[i] -> array(roomTypeName, price)
     * @return int
     * @throws \Exception
     * @deprecated
     */
    function addRoomTypes($hotelID, array $roomTypes) {
        $this->checkRole('ROLE_MANAGER');
        $count = 0;

        $this->db->beginTransaction();
        try {
            foreach ($roomTypes as $roomType) {
                $count += $this->addRoomType($hotelID, $roomType[0], $roomType[1]);
            }
            $this->db->commit();
        }
        catch(\Exception $e) {
            $this->db->rollback();
            throw $e;
        }
        return $count;
    }

    function getRoomTypes($hotelID) {
        $this->checkRole('ROLE_MANAGER');

        $template = 'SELECT *
                    FROM RoomType
                    WHERE hotelId = ?;';

        return $this->db->fetchAll($template, array($hotelID), array(\PDO::PARAM_INT));
    }

    function addRoomType($hotelID, $roomTypeName, $price, $description) {
        $this->checkRole('ROLE_MANAGER_SUBTYPE');
        $template = 'INSERT INTO RoomType
                    VALUES
                    (?, ?, ?, ?);';

        return $this->db->insert($template, array($roomTypeName, $hotelID, $price, $description),
            array(\PDO::PARAM_STR, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_STR));
    }

    function editRoomType($hotelID, $oldRoomTypeName, $newRoomTypeName, $price, $description) {
        $this->checkRole('ROLE_MANAGER');
        $template = 'UPDATE RoomType 
                    SET roomTypeName = ?, price = ?, description = ?
                    WHERE hotelID = ? AND roomTypeName = ?;';
        return $this->db->update($template, array($newRoomTypeName, $price, $description, $hotelID, $oldRoomTypeName),
            array(\PDO::PARAM_STR, \PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_INT, \PDO::PARAM_STR));
    }

    function removeRoomType($hotelID, $roomTypeName) {
        $this->checkRole('ROLE_MANAGER');
        $template = 'DELETE 
                    FROM RoomType
                    WHERE hotelID = ? AND roomTypeName = ?;';
        return $this->db->delete($template, array($hotelID, $roomTypeName), array(\PDO::PARAM_INT, \PDO::PARAM_STR));
    }

    function getAllRooms($hotelID) {
        if(!($this->app['security.authorization_checker']->isGranted('IS_AUTHENTICATED_FULLY')
            && ($this->app['security.authorization_checker']->isGranted('ROLE_MANAGER') ||
                $this->app['security.authorization_checker']->isGranted('ROLE_RECEPTIONIST'))))
            throw new AccessDeniedException();

        $template = 'SELECT
            e.roomNumber       AS roomNumber,
            e.hotelId          AS hotelId,
            e.roomTypeName     AS roomTypeName,
            e.status           AS status,
            s.firstName           AS firstName,
            s.lastName            AS lastName,
            s.middleName          AS middleName,
            r.id                  AS reservationID,
            r.isPaid              AS isPaid,
            r.isCanceled          AS isCanceled,
            r.checkInDate         AS checkInDate,
            r.checkOutDate        AS checkOutDate,
            r.reserverEmail       AS reserverEmail,
            r.reserverPhoneNumber AS reserverPhoneNumber
        FROM (
            SELECT * FROM Room WHERE Room.hotelID = ?) as e
            LEFT JOIN
                (Stay AS s INNER JOIN 
                    (SELECT *  FROM Reservation 
                    WHERE Reservation.hotelID = ? 
                        AND Reservation.isCanceled = FALSE
                        AND Reservation.checkInDate <= CURDATE() 
                        AND Reservation.checkOutDate >= CURDATE()) AS r 
                ON s.reservationID = r.id) 
            USING (roomNumber)
        WHERE r.hotelID IS NULL OR e.hotelId = r.hotelId;';

        return $this->db->fetchAll($template, array($hotelID, $hotelID), array(\PDO::PARAM_INT, \PDO::PARAM_INT));

    }

    function getRooms($hotelID) {
        $this->checkRole('ROLE_MANAGER');

        $template = 'SELECT *
                    FROM Room
                    WHERE hotelID = ?;';

        return $this->db->fetchAll($template, array($hotelID), array(\PDO::PARAM_INT));
    }

    function addRoom($roomNumber, $hotelID, $roomTypeName, $status) {
        $this->checkRole('ROLE_MANAGER_SUBTYPE');

        $template = 'INSERT INTO Room
                    VALUES
                    (?, ?, ?, ?);';

        return $this->db->insert($template, array($roomNumber, $hotelID, $roomTypeName, $status),
            array(\PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_INT));
    }

    function editRoom($hotelID, $oldRoomNumber, $newRoomNumber, $roomTypeName, $status) {
        $this->checkRole('ROLE_MANAGER');
        $template = 'UPDATE Room
                    SET roomNumber = ?, roomTypeName = ?, status = ?
                    WHERE roomNumber = ? AND hotelID = ?;';

        return $this->db->update($template, array($newRoomNumber, $roomTypeName, $status, $oldRoomNumber, $hotelID),
            array(\PDO::PARAM_STR, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT));
    }

    function removeRoom($hotelID, $roomNumber) {
        $this->checkRole('ROLE_MANAGER');
        $template = 'DELETE 
                    FROM Room
                    WHERE roomNumber = ? AND hotelID = ?;';

        return $this->db->delete($template, array($roomNumber, $hotelID), array(\PDO::PARAM_INT, \PDO::PARAM_INT));
    }

    function getAccounts($hotelID) {
        $this->checkRole('ROLE_MANAGER');

        $template = 'SELECT login
                    FROM Hotel, WebSiteUser
                    WHERE id = ? AND receptionistLogin = login;';

        return $this->db->fetchAssoc($template, array($hotelID), array(\PDO::PARAM_INT));
    }

    function getRandomManagedHotel() {
        $this->checkRole('ROLE_MANAGER');

        $login = null;
        $token = $this->app['security.token_storage']->getToken();
        if (null !== $token) {
            $login = strtolower($token->getUser()->getUsername());
        }

        $template = 'SELECT id
                    FROM Hotel
                    WHERE managerLogin = ? LIMIT 1;';
        $data = $this->db->fetchAssoc($template, array($login), array(\PDO::PARAM_STR));
        if ($data)
            return $data['id'];
        else
            return null;
    }

    function cancelReservation($reservationId) {
        $this->checkRole('ROLE_MANAGER_SUBTYPE');

        $template = 'UPDATE Reservation 
                    SET isCanceled = TRUE
                    WHERE id = ?;';
        $this->db->update($template, array($reservationId), array(\PDO::PARAM_INT));
    }

    function getReservationsFromToday($hotelId) {
        $this->checkOneOfRoles('ROLE_RECEPTIONIST', 'ROLE_MANAGER');
        $template = 'SELECT ReservationView.*, SUM(ReservationView.amount) as amount    
                    FROM ReservationView                     
                    WHERE hotelId = ? AND checkInDate >= CURDATE()
                    GROUP BY reservationID;';
        return $this->db->fetchAll($template, array($hotelId), array(\PDO::PARAM_INT));
    }

    function getAllRoomTypes($hotelId) {
        $this->checkOneOfRoles('ROLE_RECEPTIONIST', 'ROLE_MANAGER');
        $sql="SELECT t.description, t.roomTypeName, COUNT(*) as count, t.price as price
                FROM Hotel h, RoomType t, Room r
                WHERE h.id = ? AND h.id = t.hotelId
                    AND r.hotelID = h.id AND r.roomTypeName = t.roomTypeName
                GROUP BY t.roomTypeName;";
        return $this->db->fetchAll($sql,
            array($hotelId),
            array(\PDO::PARAM_INT));
    }

    function getAllRoomReservationsByRID($hotelId) {
        $this->checkOneOfRoles('ROLE_RECEPTIONIST', 'ROLE_MANAGER');
        $sql="SELECT res.id as reservationId, rr.id as roomReservationId, t.roomTypeName, rr.amount as amount
                FROM Hotel h, RoomType t, Reservation res, RoomReservation rr
                WHERE h.id = ? AND h.id = res.hotelID AND  h.id = t.hotelId
                    AND res.hotelID = h.id AND rr.reservationID = res.id
                    AND rr.roomTypeName = t.roomTypeName;";
        return $this->db->fetchAll($sql, array($hotelId), array(\PDO::PARAM_INT));
    }
}