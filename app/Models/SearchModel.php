<?php

namespace app\Model;

class SearchModel extends BaseModel {

    function getAllCountriesAndCities() {
        $template = 'SELECT c.name AS country, s.name AS state, ct.name AS city, 
                    c.id AS countryId, s.id AS stateId, ct.id AS cityId 
                    FROM Country c, State s, City ct 
                    WHERE c.id = s.countryID AND s.id = ct.stateID;';
        return $this->db->fetchAll($template);
    }

    function getDestinationsAsString() {
        if (!$data = $this->getAllCountriesAndCities())
            return '';
        $destination = '';
        foreach ($data as $d) {
            $string = $d['city'] . ', ' . $d['state'] . ', ' . $d['country'];
            $destination .= '{name: "' . $string . '", countryId: ' . $d['countryId'] . ', stateId: ' . $d['stateId'] . ', cityId: ' . $d['cityId'] . '},';
        }
        return $destination;
    }

    function getHotelInfo($hotelID) {
        $template = 'SELECT id, name, country, city, longitude, latitude, budget, description, picture
                    FROM Hotel WHERE id = ?;';
        return $this->db->fetchAssoc($template, array($hotelID), array(\PDO::PARAM_INT));
    }

    function getReservationIdIfUserHasAccess($email, $pin) {
        $template = 'SELECT id, COUNT(*) AS count    
                    FROM Reservation
                    WHERE reserverEmail = ? AND PIN = ?;';
        $data = $this->db->fetchAssoc($template, array($email, $pin), array(\PDO::PARAM_STR, \PDO::PARAM_INT));
        if ($data['count'] >0)
            return $data['id'];
        return null;
    }

    function getReservationInfo($reservationID) {
        $template = 'SELECT *    
                    FROM ReservationView 
                    WHERE reservationID = ?;';
        return $this->db->fetchAssoc($template, array($reservationID), array(\PDO::PARAM_INT));
    }

    /**
     * @param $cid
     * @param $cod
     * @param $hotelID
     * @param $reserverEmail
     * @param $reserverPhoneNumber
     * @param $isPaid
     * @param array $rooms , $rooms[i] => array(roomTypeName, amount)
     * @return string
     * @throws \Exception
     */
    function addReservation($cid, $cod, $hotelID, $reserverEmail, $reserverPhoneNumber, $isPaid, array $rooms) {
        $PIN = rand(10000, 99999);
        $this->db->beginTransaction();
        try {
            $template = 'INSERT INTO Reservation 
                    (checkInDate, checkOutDate, hotelID, reserverEmail, reserverPhoneNumber, PIN, isPaid)
                    VALUES
                    (?, ?, ?, ?, ?, ?, ?);';
            $this->db->insert($template, array($cid, $cod, $hotelID, $reserverEmail, $reserverPhoneNumber, $PIN, $isPaid),
                array(\PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_INT, \PDO::PARAM_STR,
                    \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_BOOL));

            $reservationID = $this->db->lastInsertId();
            foreach ($rooms as $room) {
                $this->addRoomReservation($reservationID, $room[0], $room[1]);
            }
            $this->db->commit();
        }
        catch (\Exception $e) {
            $this->db->rollback();
            throw $e;
        }

        return $reservationID;
    }

    /**
     * @param $id
     * @param $oldemail
     * @param $email
     * @param $pin
     * @param $reserverPhoneNumber
     * @param array $rooms , $rooms[i] => array(roomTypeName, amount)
     * @return mixed
     * @throws \Exception
     */
    function updateReservationByUser($id, $oldemail, $email, $pin, $reserverPhoneNumber, array $rooms) {
        $template = 'SELECT COUNT(*) as count 
                    FROM Reservation
                    WHERE id = ? AND reserverEmail = ? AND PIN = ?;';
        if (!($this->db->fetchAssoc($template, array($id, $oldemail, $pin), array(\PDO::PARAM_INT,\PDO::PARAM_STR, \PDO::PARAM_INT))['count'] > 0))
            throw new \Exception("Access denied!");

        // User have an access to update
        $this->db->beginTransaction();
        try {
            $template = 'UPDATE Reservation 
                        SET reserverEmail = ?, 
                        reserverPhoneNumber = ?
                        WHERE id = ?;';
            $this->db->update($template,
                array($email, $reserverPhoneNumber, $id),
                array(\PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_INT));

            $template = 'DELETE 
                        FROM RoomReservation 
                        WHERE reservationID = ?;';
            $this->db->delete($template, array($id), array(\PDO::PARAM_INT));

            foreach ($rooms as $room) {
                $this->addRoomReservation($id, $room[0], $room[1]);
            }
            $this->db->commit();
        }
        catch (\Exception $e) {
            $this->db->rollback();
            throw $e;
        }
        return $id;
    }

    function cancelReservationByReserver($reservationId, $email, $pin) {
        $template = 'SELECT COUNT(*) as count 
                    FROM Reservation
                    WHERE id = ? AND reserverEmail = ? AND PIN = ?;';
        if (!($this->db->fetchAssoc($template, array($reservationId, $email, $pin), array(\PDO::PARAM_INT,\PDO::PARAM_STR, \PDO::PARAM_INT))['count']>0))
            throw new \Exception("Access denied!");

        $template = 'UPDATE Reservation 
                    SET isCanceled = TRUE
                    WHERE id = ?;';
        $this->db->update($template, array($reservationId), array(\PDO::PARAM_INT));
    }

    function addRoomReservation($reservationID, $roomTypeName, $amount) {
        $template = 'INSERT INTO RoomReservation 
                    (reservationID, roomTypeName, amount)
                    VALUES (?, ?, ?);';
        return $this->db->insert($template,
            array($reservationID, $roomTypeName, $amount),
            array(\PDO::PARAM_INT, \PDO::PARAM_STR, \PDO::PARAM_INT));
    }

    function getRoomReservations($reservationID) {
        $template = 'SELECT *    
                    FROM RoomReservation  rr, RoomType t, Reservation r
                    WHERE rr.reservationID = ? 
                        AND t.roomTypeName = rr.roomTypeName 
                        AND t.hotelId = r.hotelId 
                        AND r.id = rr.reservationID;';
        return $this->db->fetchAll($template, array($reservationID), array(\PDO::PARAM_INT));
    }

    /**
     * Returns a list of hotels, which have unoccupied rooms for the given dates,
     * available room types, count and price.
     *
     * @param $hotelId
     * @param $checkIn
     * @param $checkOut
     * @return array
     */
    function getAllUnoccupiedRooms($hotelId, $checkIn, $checkOut) {
        $sql = <<<SQL
            SELECT AllRooms.roomTypeName, AllRooms.description, (AllRooms.count - IFNULL(occupiedCount, 0)) as count, AllRooms.price
            FROM
                (SELECT t.description, t.roomTypeName, COUNT(*) as count, t.price as price
                FROM Hotel h, RoomType t, Room r
                WHERE h.id = ? AND h.id = t.hotelId
                    AND r.hotelID = h.id AND r.roomTypeName = t.roomTypeName
                GROUP BY t.roomTypeName
                ) as AllRooms
                LEFT JOIN
                (SELECT t.roomTypeName, COUNT(*) as occupiedCount
                FROM Hotel h, RoomType t, Reservation res, RoomReservation rr
                WHERE h.id = ? AND h.id = t.hotelId
                    AND res.hotelID = h.id AND rr.reservationID = res.id
                    AND rr.roomTypeName = t.roomTypeName
                    AND res.isCanceled = FALSE
                    AND ((? <= res.checkInDate AND ? >= res.checkInDate AND ? <= res.checkOutDate)
                        OR  (? <= res.checkOutDate AND ? >= res.checkOutDate AND ? >= res.checkInDate)
                        OR (? >= res.checkInDate AND ? <= res.checkInDate))
                GROUP BY t.roomTypeName
                ) as OccupiedRooms 
                USING (roomTypeName);
SQL;
        return $this->db->fetchAll($sql,
            array($hotelId, $hotelId,
                $checkIn, $checkOut, $checkOut, $checkIn,
                $checkOut, $checkIn, $checkIn, $checkOut),
            array(\PDO::PARAM_INT, \PDO::PARAM_INT,
                \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR,
                \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR));
    }

    /**
     * Returns a list of hotels, which have unoccupied rooms for the given dates,
     * number of available rooms and price.
     *
     * @param $cityId
     * @param $checkIn
     * @param $checkOut
     * @return array
     */
    function getAllHotelsWithUnoccupiedRooms($cityId, $checkIn, $checkOut, $start = 0, $limit = 7) {
        $sql = <<<SQL
            SELECT AllRooms.hotelId, AllRooms.name, AllRooms.picture, AllRooms.description, AllRooms.latitude, 
                AllRooms.longitude, (AllRooms.count - IFNULL(occupiedCount, 0)) as count, MIN(AllRooms.price) as minPrice
            FROM
                (SELECT h.id as hotelId, h.picture, h.name, h.description, h.latitude, h.longitude, t.roomTypeName, 
                    COUNT(*) as count, t.price as price
                FROM Hotel h, RoomType t, Room r
                WHERE h.city = ? AND h.id = t.hotelId
                    AND r.hotelID = h.id AND r.roomTypeName = t.roomTypeName
                GROUP BY h.id
                ) as AllRooms
                LEFT JOIN 
                (SELECT h.id as hotelId, t.roomTypeName, COUNT(*) as occupiedCount
                FROM Hotel h, RoomType t, Reservation res, RoomReservation rr
                WHERE h.city = ? AND h.id = t.hotelId
                    AND res.hotelID = h.id AND rr.reservationID = res.id
                    AND rr.roomTypeName = t.roomTypeName
                    AND res.isCanceled = FALSE
                    AND ((? <= res.checkInDate AND ? >= res.checkInDate AND ? <= res.checkOutDate)
                        OR  (? <= res.checkOutDate AND ? >= res.checkOutDate AND ? >= res.checkInDate)
                        OR (? >= res.checkInDate AND ? <= res.checkInDate))
                GROUP BY t.roomTypeName
                ) as OccupiedRooms 
                USING (hotelId, roomTypeName)
                WHERE (AllRooms.count - IFNULL(occupiedCount, 0)) > 0
                GROUP BY AllRooms.hotelId
                LIMIT ?, ?;
SQL;
        return $this->db->fetchAll($sql,
            array($cityId, $cityId,
                $checkIn, $checkOut, $checkOut, $checkIn,
                $checkOut, $checkIn, $checkIn, $checkOut,
                $start, $limit),
            array(\PDO::PARAM_INT, \PDO::PARAM_INT,
                \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR,
                \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR, \PDO::PARAM_STR,
                \PDO::PARAM_INT, \PDO::PARAM_INT));
    }
}