<?php
namespace app\Model;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class UserProvider extends BaseModel implements UserProviderInterface {

    public function loadUserByUsername($username) {
        $username = strtolower( $username );
        $sql = "SELECT * FROM WebSiteUser WHERE login = ?;";
        if (!$user = $this->db->fetchAssoc($sql, array( $username ), array(\PDO::PARAM_STR)))
            throw new UsernameNotFoundException( sprintf( 'Username "%s" does not exist.', $username ) );

        return new UserEntity(
            $user['login'],
            $user['password'],
            explode(',', $user['roles']),
            $user['firstName'],
            $user['lastName'],
            $user['middleName']);
    }

    public function refreshUser(UserInterface $user) {
        if (!$user instanceof UserEntity)
            throw new UnsupportedUserException(sprintf('Instance of "%s" are not supported'), get_class($user));

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class) {
        return $class === 'app\Model\UserEntity';
    }
}