<?php
namespace app\Model;

class MainPageModel extends BaseModel {
    /**
     * Example of using database wrapper
     */
    function getJohnHotelsWithFiveStars() {
        $template = 'SELECT * FROM hotels WHERE starts = ? AND name = ?;';
        return $this->db->fetchAssoc($template, array(5, "John hotel"), array(\PDO::PARAM_INT, \PDO::PARAM_STR));
    }
}