# Create a user hms
CREATE USER 'hms'@'localhost'
    IDENTIFIED BY 'n0rmal4m';

# Allow him to select data from all tables
GRANT SELECT ON *.* TO 'hms'@'localhost';

# Create 'hms' database
CREATE DATABASE IF NOT EXISTS hms
    CHARACTER SET utf8
    COLLATE utf8_general_ci;

# Give the user 'hms' full access to the database 'hms'
GRANT ALL PRIVILEGES ON hms TO 'hms'@'localhost';

# Update privileges
FLUSH PRIVILEGES;

#Tables

CREATE TABLE WebSiteUser (
    login      VARCHAR(100) PRIMARY KEY,
    password   VARCHAR(100),
    roles      VARCHAR(300),
    firstName  VARCHAR(50),
    lastName   VARCHAR(50),
    middleName VARCHAR(50)
);

CREATE TABLE Hotel (
    id                INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name              VARCHAR(50),
    country           INTEGER,
    state             INTEGER,
    city              INTEGER,
    longitude         DECIMAL(11, 8),
    latitude          DECIMAL(10, 8),
    budget            DOUBLE NOT NULL,
    description       TEXT,
    managerLogin      VARCHAR(100),
    ownerLogin        VARCHAR(100),
    receptionistLogin VARCHAR(100),
    picture           VARCHAR(100),
    FOREIGN KEY (managerLogin) REFERENCES WebSiteUser (login),
    FOREIGN KEY (ownerLogin) REFERENCES WebSiteUser (login),
    FOREIGN KEY (receptionistLogin) REFERENCES WebSiteUser (login),
    FOREIGN KEY (country) REFERENCES Country(id),
    FOREIGN KEY (state) REFERENCES  State(id),
    FOREIGN KEY (city) REFERENCES  City(id)
);

CREATE TABLE RoomType (
    roomTypeName VARCHAR(100) NOT NULL,
    hotelId      INTEGER,
    price        DOUBLE,
    description  VARCHAR(200),
    FOREIGN KEY (hotelId) REFERENCES Hotel (id)
        ON DELETE CASCADE,
    PRIMARY KEY (roomTypeName, hotelID)
);

CREATE TABLE Room (
    roomNumber   INTEGER,
    hotelID      INTEGER,
    roomTypeName VARCHAR(100),
    status       INTEGER NOT NULL,
    FOREIGN KEY (hotelID) REFERENCES Hotel (id)
        ON DELETE CASCADE,
    FOREIGN KEY (roomTypeName) REFERENCES RoomType (roomTypeName)
        ON DELETE NO ACTION ON UPDATE CASCADE,
    PRIMARY KEY (roomNumber, hotelID)
);

CREATE TABLE Reservation (
    id                  INTEGER AUTO_INCREMENT PRIMARY KEY,
    checkInDate         DATE    NOT NULL,
    checkOutDate        DATE    NOT NULL,
    hotelID             INTEGER,
    reserverEmail       VARCHAR(255),
    reserverPhoneNumber VARCHAR(31),
    PIN                 INTEGER,
    isCanceled          BOOLEAN NOT NULL,
    isPaid              BOOLEAN NOT NULL,
    FOREIGN KEY (hotelID) REFERENCES Hotel (id)
        ON DELETE CASCADE
);

CREATE TABLE RoomReservation (
    id            INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    reservationID INTEGER,
    roomTypeName  VARCHAR(100),
    amount        INTEGER,
    actualAmount  INTEGER,
    FOREIGN KEY (reservationID) REFERENCES Reservation (id)
        ON DELETE CASCADE,
    FOREIGN KEY (roomTypeName) REFERENCES RoomType (roomTypeName)
        ON DELETE NO ACTION ON UPDATE CASCADE
);

CREATE TABLE Stay (
    reservationID  INTEGER,
    roomNumber     INTEGER,
    firstName      VARCHAR(50),
    lastName       VARCHAR(50),
    middleName     VARCHAR(50),
    IDDocumentScan BLOB,
    PRIMARY KEY (reservationID, roomNumber),
    FOREIGN KEY (reservationID) REFERENCES Reservation (id)
        ON DELETE CASCADE,
    FOREIGN KEY (roomNumber) REFERENCES Room (roomNumber)
        ON DELETE NO ACTION ON UPDATE CASCADE
);

CREATE TABLE Country (
    id INTEGER PRIMARY KEY NOT NULL,
    shortName VARCHAR(3) NOT NULL,
    name VARCHAR(150) NOT NULL
);

CREATE TABLE State (
    id INTEGER PRIMARY KEY NOT NULL,
    name VARCHAR(30) NOT NULL,
    countryID INTEGER NOT NULL
);

CREATE TABLE City(
    id INTEGER PRIMARY KEY NOT NULL,
    name VARCHAR(30) NOT NULL,
    stateID INTEGER NOT NULL
);

CREATE TABLE Employee (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    hotelID INTEGER,
    firstName VARCHAR(50) NOT NULL,
    lastName VARCHAR(50) NOT NULL,
    middleName VARCHAR(50),
    position VARCHAR(50) NOT NULL,
    salary DOUBLE NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (hotelID) REFERENCES Hotel(id) ON DELETE CASCADE
);

#Views

CREATE VIEW ReservationView AS
    SELECT
        h.id                  AS hotelId,
        r.id                  AS reservationID,
        r.checkInDate         AS checkInDate,
        r.checkOutDate        AS checkOutDate,
        r.reserverPhoneNumber AS reserverPhoneNumber,
        r.reserverEmail       AS reserverEmail,
        r.PIN                 AS PIN,
        r.isCanceled          AS isCanceled,
        r.isPaid              AS isPaid,
        rr.amount             AS amount,
        rr.actualAmount       AS actualAmount
    FROM Reservation r, RoomReservation rr, Hotel h
    WHERE r.id = rr.reservationID AND r.hotelID = h.id;

# Triggers

DELIMITER |
CREATE TRIGGER reserverEmailCheck BEFORE INSERT ON Reservation
FOR EACH ROW
    BEGIN
        DECLARE err_msg VARCHAR(255);
        IF NEW.reserverEmail NOT LIKE '%@%.%'
        THEN
            SET err_msg = concat('E-mail format violated: ', NEW.reserverEmail);
            SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = err_msg;
        END IF;
    END;
|
DELIMITER ;

DELIMITER |
CREATE TRIGGER hotelCoordinatesCheck BEFORE INSERT ON Hotel
FOR EACH ROW
    BEGIN
        DECLARE msg VARCHAR(255);
        IF longitude < -180 OR longitude > 180
        THEN
            SET msg = concat('Constraint hotelCoordinates violated: longitude should be in range [-180;180], found: ',
                             cast(NEW.longitude AS CHAR));
            SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = msg;
        END IF;
        IF latitude < -90 OR latitude > 90
        THEN
            SET msg = concat('Constraint hotelCoordinates violated: latitude should be in range [-90;90], found: ',
                             cast(NEW.latitude AS CHAR));
            SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = msg;
        END IF;
    END;
|
DELIMITER ;

DELIMITER |
CREATE TRIGGER roomReservationAmountCheck BEFORE INSERT ON RoomReservation
FOR EACH ROW
    BEGIN
        DECLARE msg VARCHAR(255);
        IF NEW.amount <= 0
        THEN
            SET msg = concat('Constraint roomReservationAmount violated: amount should be greater than zero, found: ',
                             cast(NEW.amount AS CHAR));
            SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = msg;
        END IF;
    END;
|
DELIMITER ;

DELIMITER |
CREATE TRIGGER setRoomOccupiedOnStayInsertion AFTER INSERT ON Stay
FOR EACH ROW
    # Set room status 'occupied'
    UPDATE Room
    SET status = 0
    WHERE roomNumber = NEW.roomNumber
          AND hotelID =
              (SELECT r.hotelID
               FROM Reservation r
               WHERE r.id = NEW.reservationID);
|
DELIMITER ;