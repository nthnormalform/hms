<?php

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/MyApplication.php';

$app = new MyApplication();
$app['debug'] = true;

require_once 'routes.php';
$app->run();
