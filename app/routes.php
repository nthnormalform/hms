<?php

use Symfony\Component\HttpFoundation\Request;
Request::enableHttpMethodParameterOverride();

class MyRoute extends Silex\Route {
    use Silex\Route\SecurityTrait;
}

$app->mount("/", new \app\Controller\SearchController());
$app->mount("/", new \app\Controller\AuthController());
$app->mount("/admin", new \app\Controller\AdminController());
$app->mount("/manage", new \app\Controller\ManagerController());
$app->mount("/reception", new \app\Controller\ReceptionController());