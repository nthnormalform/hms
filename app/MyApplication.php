<?php

use Silex\Application;

require_once __DIR__.'/config.php';

class MyApplication extends Application {
    use Application\TwigTrait;
    use Application\MonologTrait;
    use Application\SecurityTrait;
    use Application\UrlGeneratorTrait;

    function __construct(array $values = array()) {
        parent::__construct($values);
        $app['route_class'] = 'MyRoute';
        $this->loadServiceProviders();
        $this->setSecurityRules();
        $this->loadModels();
    }

    /**
     * This function loads ServiceProviders
     */
    function loadServiceProviders() {
        $this->register(new Silex\Provider\TwigServiceProvider(), array('twig.path' => __DIR__ . '/Views'));
        $this->register(new Silex\Provider\MonologServiceProvider(), array('monolog.logfile' => __DIR__ . '/development.log'));
        $this->register(new Silex\Provider\DoctrineServiceProvider(), array(
            'db.options' => array (
                'driver'    => 'pdo_mysql',
                'host'      => MYSQL_HOST,
                'dbname'    => MYSQL_DATABASE,
                'user'      => MYSQL_USER,
                'password'  => MYSQL_PASSWORD,
                'charset'   => 'utf8'
            )
        ));
    }

    /**
     * This function sets security rules
     */
    function setSecurityRules() {
        $this->register(new Silex\Provider\SessionServiceProvider());
        $this->register(new Silex\Provider\SecurityServiceProvider(), array(
            'security.firewalls' => array(
                'secured' => array(
                    'pattern' => '^/secured',
                    'form'    => array(
                        'login_path'         => '/login',
                        'check_path'         => '/secured/login_check',
                        'always_use_default_target_path' => true,
                        'default_target_path' => '/login/redirect',
                        'username_parameter' => '_username',
                        'password_parameter' => '_password',
                        'use_referer'        => true,
                    ),
                    'anonymous' => false,
                    'logout'    => array('logout_path' => '/secured/logout'),
                    'users'     => function () {
                        return new app\Model\UserProvider($this);
                    },
                ),
                'all' => array(
                    'pattern'   => '^/.*$',
                    'anonymous' => true,
                    'context'   =>'secured',  // This allows us to use same users for different firewalls
                ),
            ),
        ));

        $this['security.role_hierarchy'] = array(
            'ROLE_ADMIN' => array('ROLE_USER', 'ROLE_CLIENT', 'ROLE_PASSWORD_CHANGE', 'ROLE_MANAGER_SUBTYPE', 'ROLE_RECEPTIONIST_SUBTYPE'),
            'ROLE_MANAGER' => array('ROLE_CLIENT', 'ROLE_PASSWORD_CHANGE', 'ROLE_FULL_ROOM_INFO_VIEWER', 'ROLE_MANAGER_SUBTYPE'),
            'ROLE_RECEPTIONIST' => array('ROLE_CLIENT', 'ROLE_FULL_ROOM_INFO_VIEWER', 'ROLE_RECEPTIONIST_SUBTYPE'),
            'ROLE_CLEANER' => array('ROLE_CLIENT'),
            'ROLE_WORKER' => array('ROLE_CLIENT'),
        );

        $this['security.access_rules'] = array(
            array('^/admin', 'ROLE_ADMIN'),
            array('^/manage', 'ROLE_MANAGER'),
            array('^/reception', 'ROLE_RECEPTIONIST'),
            array('^/cleaner', 'ROLE_CLEANER'),
            array('^/work', 'ROLE_WORKER'),
        );
    }

    /**
     * This function loads all models
     */
    function loadModels() {
        $this['db_wrapper'] = function() {
            return new app\Model\DatabaseWrapper($this);
        };
        $this['user_management'] = function() {
            return new app\Model\UserManagementModel($this);
        };
        $this['user_provider'] = function() {
            return new app\Model\UserProvider($this);
        };
        $this['search_model'] = function() {
            return new app\Model\SearchModel($this);
        };
        $this['manager_model'] = function() {
            return new app\Model\ManagerModel($this);
        };
        $this['admin_model'] = function() {
            return new app\Model\AdminModel($this);
        };
    }

    /**
     * Used in Controllers
     * @return \app\Model\DatabaseWrapper
     */
    function db() {
        return $this['db_wrapper'];
    }

    /**
     * Shows error flash message
     * @param $msg
     */
    function flashError($msg) {
        $this['session']->getFlashBag()->add('error', $msg);
    }

    /**
     * Shows warning flash message
     * @param $msg
     */
    function flashWarning($msg) {
        $this['session']->getFlashBag()->add('warning', $msg);
    }

    /**
     * Shows info flash message
     * @param $msg
     */
    function flashInfo($msg) {
        $this['session']->getFlashBag()->add('info', $msg);
    }

    /**
     * Shows success flash message
     * @param $msg
     */
    function flashSuccess($msg) {
        $this['session']->getFlashBag()->add('success', $msg);
    }
}