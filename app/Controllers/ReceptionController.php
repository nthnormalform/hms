<?php
namespace app\Controller;

use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class ReceptionController extends BaseController implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app["controllers_factory"];
        $controllers->get("/", "app\\Controller\\ReceptionController::roomStatus")
            ->bind('reception');

        $controllers->post("/checkin2", "app\\Controller\\ReceptionController::checkInAndCreateReservation")
            ->bind('reception_checkin_and_create_reservation');
        $controllers->post("/checkin", "app\\Controller\\ReceptionController::checkIn")
            ->bind('reception_checkin');
        $controllers->post("/checkout", "app\\Controller\\ReceptionController::checkOut")
            ->bind('reception_checkout');
        $controllers->get("/reservation", "app\\Controller\\ReceptionController::reservations")
            ->bind('reception_reservations');
        $controllers->post("/reservation", "app\\Controller\\ReceptionController::addReservation")
            ->bind('reception_add_reservation');
        $controllers->put("/reservation", "app\\Controller\\ReceptionController::editReservation")
            ->bind('reception_edit_reservation');
        $controllers->delete("/reservation", "app\\Controller\\ReceptionController::cancelReservation")
            ->bind('reception_cancel_reservation');
        return $controllers;
    }

    public function roomStatus(\MyApplication $app, Request $request) {
        $hotelId = $app['session']->get('hotelId');
        $occupiedRooms = $app['manager_model']->getAllRooms($hotelId);
        $availableReservations = $app['manager_model']->getReservationsAvailableForCheckIn($hotelId);

        $statuses = array(0=>'Occupied',
            1=>'Unoccupied',
            2=>'Dirty',
            3=>'Cleaning',
            4=>'Unavailable',
            5=>'Need maintenance',
            6=>'Under maintenance');
        $roomStatuses = $app['manager_model']->getRoomStatuses($hotelId);
        $newRoomStatuses = array();

        for ($i = 0; $i <= 6; $i++) {
            $found = FALSE;
            for ($j = 0; $j < count($roomStatuses); $j++) {
                if (!is_null($roomStatuses[$j]) && $roomStatuses[$j]['status'] == $i) {
                    $newRoomStatuses[$i] = $roomStatuses[$j]['count'];
                    $found = TRUE;
                    break;
                }
            }
            if (!$found) $newRoomStatuses[$i] = '0';
        }

        return $app->render('reception.twig', array('hotelid'=>$hotelId, 'rooms'=>$occupiedRooms,
            'statuses'=>$statuses, 'roomstatuses'=>$newRoomStatuses, 'reservations'=>$availableReservations));
    }

    /*
     * Check in using existing reservation
     */
    public function checkIn(\MyApplication $app, Request $request) {
        $roomNumber = $request->get('roomnumber');
        $reservationId = $request->get('reservationid');
        $firstName = $request->get('firstname');
        $lastName = $request->get('lastname');
        $middleName = $request->get('middlename'); //  allow it to be empty or null

        if ($this->bn(array($roomNumber))) {
            $app->flashError("Wrong room number!");
            return $app->redirect($app->path('reception'));
        }
        if ($this->bs(array($firstName, $lastName))) {
            $app->flashError("Please specify first name and last name!");
            return $app->redirect($app->path('reception'));
        }

        $app['manager_model']->checkIn($reservationId, $roomNumber, $firstName, $lastName, $middleName);
        $app->flashSuccess("Successfully checked in! Room number: $roomNumber, $firstName $lastName!");
        return $app->redirect($app->path('reception'));
    }

    /*
     * Check in without existing reservation
     */
    public function checkInAndCreateReservation(\MyApplication $app, Request $request) {
        $hotelId = $app['session']->get('hotelId');
        $roomNumber = $request->get('roomnumber');
        $firstName = $request->get('firstname');
        $lastName = $request->get('lastname');
        $middleName = $request->get('middlename'); //  allow it to be empty or null
        $email = $request->get('email');
        $phone = $request->get('phone');

        $checkInDate = $request->get('checkindate');
        $checkOutDate = $request->get('checkoutdate');
        $isPaid = $request->get('ispaid') == 'on'?true:false;

        if ($this->bn(array($hotelId)))
            throw new Exception("Internal error: hotel is undefined!");
        if ($this->bn(array($roomNumber))) {
            $app->flashError("Wrong room number!");
            return $app->redirect($app->path('reception'));
        }
        if ($this->bs(array($firstName, $lastName, $email, $phone))) {
            $app->flashError("Please specify first name, last name, e-mail and phone number!");
            return $app->redirect($app->path('reception'));
        }
        if ($this->bs(array($checkInDate, $checkOutDate))) {
            $app->flashError("Please specify dates!");
            return $app->redirect($app->path('reception'));
        }

        $app['manager_model']->checkInWithoutReservation($hotelId, $checkInDate, $checkOutDate, $email, $phone, $isPaid, $roomNumber, $firstName, $lastName, $middleName);
        $app->flashSuccess("Successfully checked in! Room number: $roomNumber, $firstName $lastName!");
        return $app->redirect($app->path('reception'));
    }

    public function checkOut(\MyApplication $app, Request $request) {
        $hotelId = $app['session']->get('hotelId');
        if ($this->bn(array($hotelId)))
            throw new Exception("Internal error: hotel is undefined!");

        $roomNumber = $request->get('roomnumber');
        if ($this->bn(array($roomNumber))) {
            $app->flashError("Wrong room number!");
            return $app->redirect($app->path('reception'));
        }
        $app['manager_model']->checkOut($hotelId, $roomNumber);
        $app->flashSuccess("Room ".$roomNumber." was successfully checked out!");
        return $app->redirect($app->path('reception'));
    }

    /*
     * Reservations
     */
    public function reservations(\MyApplication $app, Request $request) {
        $hotelId = $app['session']->get('hotelId');
        $reservations = $app['manager_model']->getReservationsFromToday($hotelId);
        $roomTypes = $app['manager_model']->getAllRoomTypes($hotelId);

        $roomReservations = $app['manager_model']->getAllRoomReservationsByRID($hotelId);

        return $app->render('reception_reservations.twig',
            array('reservations' => $reservations, 'roomtypes'=>$roomTypes, 'roomReservations'=>$roomReservations));
    }

    public function addReservation(\MyApplication $app, Request $request) {
        $hotelId = $app['session']->get('hotelId');

        $checkInDate = $request->get('checkindate');
        $checkOutDate = $request->get('checkoutdate');
        $isPaid = $request->get('ispaid') == 'on'?true:false;

        $email = $request->get('email');
        $phone = $request->get('phone');
        $count = $request->get('count');  // Look how it's implemented in hotel.twig
        $roomTypes = $request->get('roomtypename');

        if ($this->bn(array($hotelId)))
            throw new Exception("Internal error: hotel is undefined!");

        if ($this->bs(array($checkInDate, $checkOutDate, $email, $phone))) {
            $app->flashError("Please specify dates, phone,  name and whether is paid!");
            return $app->redirect($app->path('reception_reservations'));
        }

        if (count($count) != count($roomTypes)) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('reception_reservations'));
        }

        $rooms = array();
        for($i = 0; $i < count($count); $i++) {
            if ($count[$i] > 0)
                $rooms[$i] = array($roomTypes[$i], $count[$i]);
        }

        $reservationId = $app['search_model']->addReservation($checkInDate, $checkOutDate, $hotelId, $email, $phone, $isPaid, $rooms);
        $app->flashSuccess("New reservation was successfully added!");
        return $app->redirect($app->path('reception_reservations'));
    }

    public function editReservation(\MyApplication $app, Request $request) {
        $email = $request->get('email');
        $phone = $request->get('phone');
        $reservationId = $request->get('id');
        $isPaid = $request->get('ispaid') == 'on'?true:false;

        // FIXME: here should be checking of 'count' and 'roomTypes'
        $count = $request->get('count');
        $roomTypes = $request->get('roomtypename');

        if ($this->bs(array($email, $phone))) {
            $app->flashError("Please specify phone,  name and whether reservation is payed!");
            return $app->redirect($app->path('reception_reservations'));
        }

        if ($this->bn(array($reservationId))) {
            $app->flashError("No reservation id specified!");
            return $app->redirect($app->path('reception_reservations'));
        }

        if (count($count) != count($roomTypes)) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('reception_reservations'));
        }

        $rooms = array();
        for($i = 0; $i < count($count); $i++) {
            if ($count[$i] > 0)
                $rooms[$i] = array($roomTypes[$i], $count[$i]);
        }
        $app['manager_model']->updateReservation($reservationId, $email, $phone, $isPaid, $rooms);
        $app->flashSuccess("Reservation was successfully updated!");
        return $app->redirect($app->path('reception_reservations'));
    }

    public function cancelReservation(\MyApplication $app, Request $request) {
        $reservationId = $request->get('id');
        if ($this->bn(array($reservationId))) {
            $app->flashError("Wrong reservation id!");
            return $app->redirect($app->path('reception_reservations'));
        }

        $app['manager_model']->cancelReservation($reservationId);
        $app->flashSuccess('Reservation was successfully canceled!');
        return $app->redirect($app->path('reception_reservations'));
    }
}