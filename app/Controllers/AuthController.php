<?php
namespace app\Controller;

use app\Model\UserEntity;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class AuthController extends BaseController implements ControllerProviderInterface {
    public function connect(Application $app) {
        $auth = $app["controllers_factory"];
        $auth->match("/login/redirect", "app\\Controller\\AuthController::redirectAfterLogin")
            ->bind('login_redirect');
        $auth->match("/login", "app\\Controller\\AuthController::login")
            ->bind('login');
        $auth->match("/secured/logout", "app\\Controller\\AuthController::logout")
            ->bind('logout');
        $auth->post("/signup", "app\\Controller\\AuthController::signup")
            ->bind('signup');
        return $auth;
    }

    public function redirectAfterLogin(\MyApplication $app, Request $request) {
        if($app['security.authorization_checker']->isGranted('ROLE_ADMIN'))
            return $app->redirect($app->path('admin_statistics'));
        else if($app['security.authorization_checker']->isGranted('ROLE_MANAGER'))
            return $app->redirect($app->path('manager'));
        else if($app['security.authorization_checker']->isGranted('ROLE_RECEPTIONIST')) {
            $hotelId = $app['manager_model']->getMyHotelId();
            $app['session']->set('hotelId', $hotelId);
            return $app->redirect($app->path('reception'));
        }
        return $app->redirect($app->path('main'));
    }

    public function logout(\MyApplication $app, Request $request) {
        $app['session']->clear();
        return $app->redirect($request->headers->get('referer'));
    }

    public function login(\MyApplication $app, Request $request) {
        $lastError = $app['security.last_error']($request);
        if ($lastError)
            $app->flashError($lastError);
        return $app['twig']->render('auth.twig', array(
            'last_username' => $app['session']->get( '_security.last_username' ),
            'error'         => $lastError,
        ));
    }

    public function signup(\MyApplication $app, Request $request) {
        $username = $request->get('_username');
        $password = $request->get('_password');
        $password2 = $request->get('_password2');

        if ($this->bs(array($username, $password, $password2)) ) {
            $app->flashError('Can\'t add new user: wrong data submitted');
            return $app->redirect($app->path('login'));
        }
        else if ($password2 != $password) {
            $app->flashError('Entered passwords don\'t match!');
            return $app->redirect($app->path('login'));
        }
        // Creating an account
        $app['user_management']->addUser($username, $password, 'ROLE_MANAGER');
        // Signing in
        /** @var UserEntity $user */
        $user = $app['user_provider']->loadUserByUsername($username);
        $app['security.token_storage']->setToken(new \Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken($user, $user->getPassword(), 'all', $user->getRoles()));

        return $app->redirect($app->path('manager'));
    }
}