<?php
namespace app\Controller;

use Silex\Application;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class BaseController {
    /**
     * Used to ensure that user has access to call some method.
     * Call this when special access roles are needed.
     *
     * @param Application $app
     * @param array $roles
     */
    protected function checkRoles(Application $app, array $roles) {
        if(!$app['security.authorization_checker']->isGranted('IS_AUTHENTICATED_FULLY'))
            throw new AccessDeniedException();
        foreach ($roles as $r)
            if (!$app['security.authorization_checker']->isGranted($r))
                throw new AccessDeniedException();
    }

    /**
     * Used to ensure that user has access to call some method.
     * Call this when special access role is needed.
     *
     * @param Application $app
     * @param array $role
     */
    protected function checkRole(Application $app, $role) {
        if(!($app['security.authorization_checker']->isGranted('IS_AUTHENTICATED_FULLY')
            && $app['security.authorization_checker']->isGranted($role)))
            throw new AccessDeniedException();
    }

    /**
     * bs is a shortcut for Bad String.
     * Used to check input parameters in controllers.
     * @param array $arr
     * @return bool
     */
    protected function bs(array $arr) {
        foreach($arr as $a) {
            if (is_null($a) || $a == "")
                return true;
        }
        return false;
    }

    /**
     * bn is a shortcut for Bad Numeric.
     * Used to check input parameters in controllers.
     * @param array $arr
     * @return bool
     */
    protected function bn(array $arr) {
        foreach($arr as $a) {
            if (is_null($a) || !is_numeric($a))
                return true;
        }
        return false;
    }
}