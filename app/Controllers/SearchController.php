<?php
namespace app\Controller;

use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends BaseController implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app["controllers_factory"];
        $controllers->get("/", "app\\Controller\\SearchController::index")
            ->bind('main');
        $controllers->post("/search/morehotels", "app\\Controller\\SearchController::getMoreHotels")
            ->bind('morehotels');
        $controllers->post("/search", "app\\Controller\\SearchController::search")
            ->bind('search');
        $controllers->post("/hotel", "app\\Controller\\SearchController::hotel")
            ->bind('hotel');
        $controllers->post("/reserve/", "app\\Controller\\SearchController::reserve")
            ->bind('reserve');
        $controllers->delete("/bookinginfo", "app\\Controller\\SearchController::cancelBookingByUser")
            ->bind('cancel_booking');
        $controllers->put("/bookinginfo", "app\\Controller\\SearchController::changeBookingByUser")
            ->bind('change_booking');
        $controllers->match("/bookinginfo", "app\\Controller\\SearchController::bookinginfo")
            ->bind('bookinginfo');
        return $controllers;
    }

    public function index(\MyApplication $app, Request $request) {
        $data = $app['search_model']->getDestinationsAsString();
        return $app->render('main.twig',
            array('destinations' => $data));
    }

    public function search(\MyApplication $app, Request $request) {
        $countryId = $request->get('countryid');
        $stateId = $request->get('stateid');
        $cityId = $request->get('cityid');

        $checkInDate = $request->get('checkindate');
        $checkOutDate = $request->get('checkoutdate');
        $destination = $request->get('destination');
        if ($this->bs(array($checkInDate, $checkOutDate, $destination))) {
            $app->flashError("Please specify dates and destination!");
            return $app->redirect($app->path('main'));
        }
        if ($this->bn(array($countryId, $stateId, $cityId))) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('main'));
        }
        $hotels = $app['search_model']->getAllHotelsWithUnoccupiedRooms($cityId, $checkInDate, $checkOutDate);

        $destinationData = $app['search_model']->getDestinationsAsString();
        return $app->render('search.twig',
            array('destinations' => $destinationData,
                'hotels' => $hotels, 'checkInDate' => $checkInDate, 'checkOutDate' => $checkOutDate,
                'destination' => $destination, 'countryId'=>$countryId, 'stateId' => $stateId, 'cityId' => $cityId));
    }

    function getMoreHotels(\MyApplication $app, Request $request) {
        $countryId = $request->get('countryid');
        $stateId = $request->get('stateid');
        $cityId = $request->get('cityid');
        $startFrom = $request->get('startfrom');

        $checkInDate = $request->get('checkindate');
        $checkOutDate = $request->get('checkoutdate');

        if ($this->bs(array($checkInDate, $checkOutDate))) {
            $app->flashError("Please specify dates and destination!");
            return $app->redirect($app->path('main'));
        }

        if ($this->bn(array($countryId, $stateId, $cityId, $startFrom))) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('main'));
        }
        $startFrom = intval($startFrom);
        $hotels = $app['search_model']->getAllHotelsWithUnoccupiedRooms($cityId, $checkInDate, $checkOutDate, $startFrom);
        return $app->json($hotels);
    }

    public function hotel(\MyApplication $app, Request $request) {
        $hotelId = $request->get('id');
        $checkInDate = $request->get('checkindate');
        $checkOutDate = $request->get('checkoutdate');

        if ($this->bs(array($checkInDate, $checkOutDate))) {
            $app->flashError("Please specify dates and destination!");
            return $app->redirect($app->path('search'));
        }

        if ($this->bn(array($hotelId)) || intval($hotelId) != $hotelId) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('search'));
        }

        $hotel = $app['search_model']->getHotelInfo($hotelId);
        $roomTypes = $app['search_model']->getAllUnoccupiedRooms($hotelId, $checkInDate, $checkOutDate);

        return $app->render('hotel.twig', array('hotel' => $hotel,
            'roomtypes' => $roomTypes,
            'checkInDate' => $checkInDate,
            'checkOutDate' => $checkOutDate));
    }

    public function reserve(\MyApplication $app, Request $request) {
        $hotelId = $request->get('hotelid');
        $checkInDate = $request->get('checkindate');
        $checkOutDate = $request->get('checkoutdate');

        $email = $request->get('email');
        $phone = $request->get('phone');
        // FIXME: here should be checking of 'count' and 'roomTypes'
        $count = $request->get('count');
        $roomTypes = $request->get('roomtypename');

        if ($this->bs(array($checkInDate, $checkOutDate, $email, $phone))) {
            $app->flashError("Please specify dates, phone and name!");
            return $app->redirect($app->path('hotel'));
        }

        if ($this->bn(array($hotelId)) || intval($hotelId) != $hotelId) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('hotel'));
        }

        if (count($count) != count($roomTypes)) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('hotel'));
        }

        $rooms = array();
        for($i = 0; $i < count($count); $i++) {
            if ($count[$i] > 0)
                $rooms[$i] = array($roomTypes[$i], $count[$i]);
        }
        $reservationId = $app['search_model']->addReservation($checkInDate, $checkOutDate, $hotelId, $email, $phone, false, $rooms);

        $params = $request->request->keys();
        $params['rid'] = $reservationId;
        if (is_null($app['session']->get('justAddedReservations'))) {
            $app['session']->set('justAddedReservations', array($reservationId));
        }
        else {
            $ids = $app['session']->get('justAddedReservations');
            array_push($ids, $reservationId);
            $app['session']->set('justAddedReservations', $ids);
        }
        $app['session']->set('lastReservation', $reservationId);
        $app->flashSuccess("You have successfully booked it!");
        return $app->redirect($app->path('bookinginfo'));
    }

    public function bookinginfo(\MyApplication $app, Request $request) {
        $reservationId = null;
        // User just added the booking
        if ($request->getMethod() == 'GET') {
            $reservationId = $app['session']->get('lastReservation');
            if(!in_array($reservationId, $app['session']->get('justAddedReservations')))
                return $app->redirect($app->path('main'));
            // TODO: remove last reservation id from session?
        }
        // Or entered email and pin
        else if ($request->getMethod() == 'POST') {
            $email = $request->get('email');
            $pin = $request->get('pin');

            if ($this->bs(array($email))) {
                $app->flashError("Wrong email!");
                return $app->redirect($app->path('main'));
            }
            if ($this->bn(array($pin))) {
                $app->flashError("Wrong pin!");
                return $app->redirect($app->path('main'));
            }
            $reservationId = $app['search_model']->getReservationIdIfUserHasAccess($email, $pin);
            if (is_null($reservationId)) {
                $app->flashError("Please check your email and PIN!");
                return $app->redirect($app->path('main'));
            }
        }

        $reservationInfo = $app['search_model']->getReservationInfo($reservationId);
        $roomReservations = $app['search_model']->getRoomReservations($reservationId);

        $hotel = $app['search_model']->getHotelInfo($reservationInfo['hotelId']);
        $roomTypes = $app['search_model']->getAllUnoccupiedRooms($reservationInfo['hotelId'], $reservationInfo['checkInDate'], $reservationInfo['checkOutDate']);

        return $app->render('bookinginfo.twig',
            array('hotel' => $hotel,
                'reservation' => $reservationInfo,
                'roomtypes' => $roomTypes,
                'roomReservations' => $roomReservations));
    }

    public function cancelBookingByUser(\MyApplication $app, Request $request) {
        $email = $request->get('email');
        $pin = $request->get('pin');
        $reservationId = $request->get('reservationid');

        if ($this->bs(array($email))) {
            $app->flashError("Please specify an e-mail!");
            return $app->redirect($app->path('hotel'));
        }

        if ($this->bn(array($pin, $reservationId))) {
            $app->flashError("Internal error :(!");
            return $app->redirect($app->path('hotel'));
        }

        $app['search_model']->cancelReservationByReserver( $reservationId, $email, $pin);
        $app->flashSuccess("Your booking was successfully canceled!");

        $reservationInfo = $app['search_model']->getReservationInfo($reservationId);
        $roomReservations = $app['search_model']->getRoomReservations($reservationId);

        $hotel = $app['search_model']->getHotelInfo($reservationInfo['hotelId']);
        $roomTypes = $app['search_model']->getAllUnoccupiedRooms($reservationInfo['hotelId'], $reservationInfo['checkInDate'], $reservationInfo['checkOutDate']);

        return $app->render('bookinginfo.twig',
            array('hotel' => $hotel,
                'reservation' => $reservationInfo,
                'roomtypes' => $roomTypes,
                'roomReservations' => $roomReservations));
    }

    public function changeBookingByUser(\MyApplication $app, Request $request) {
        $email = $request->get('email');
        $oldemail = $request->get('oldemail');
        $phone = $request->get('phone');
        $pin = $request->get('pin');
        $reservationId = $request->get('reservationid');
        $hotelId = $request->get('hotelid');

        // FIXME: here should be checking of 'count' and 'roomTypes'
        $count = $request->get('count');
        $roomTypes = $request->get('roomtypename');

        if ($this->bs(array($oldemail, $email, $phone))) {
            $app->flashError("Please specify phone and name!");
            return $app->redirect($app->path('hotel'));
        }

        if ($this->bn(array($hotelId)) || intval($hotelId) != $hotelId) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('hotel'));
        }

        if (count($count) != count($roomTypes)) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('hotel'));
        }

        $rooms = array();
        for($i = 0; $i < count($count); $i++) {
            if ($count[$i] > 0)
                $rooms[$i] = array($roomTypes[$i], $count[$i]);
        }
        $app['search_model']->updateReservationByUser( $reservationId, $oldemail, $email, $pin, $phone, $rooms);
        $app->flashSuccess("Your booking was successfully updated!");

        $reservationInfo = $app['search_model']->getReservationInfo($reservationId);
        $roomReservations = $app['search_model']->getRoomReservations($reservationId);

        $hotel = $app['search_model']->getHotelInfo($reservationInfo['hotelId']);
        $roomTypes = $app['search_model']->getAllUnoccupiedRooms($reservationInfo['hotelId'], $reservationInfo['checkInDate'], $reservationInfo['checkOutDate']);

        return $app->render('bookinginfo.twig',
            array('hotel' => $hotel,
                'reservation' => $reservationInfo,
                'roomtypes' => $roomTypes,
                'roomReservations' => $roomReservations));
    }
}
