<?php
namespace app\Controller;

use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends BaseController implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app["controllers_factory"];
        $controllers->get("/", "app\\Controller\\AdminController::statistics")
            ->bind('admin_statistics');
        $controllers->get("/performance", "app\\Controller\\AdminController::performance")
            ->bind('admin_performance');
        $controllers->get("/generate", "app\\Controller\\AdminController::generate")
            ->bind('admin_generate');
        return $controllers;
    }

    public function statistics(\MyApplication $app, Request $request) {
        return $app->render('common.twig');
    }
    
    public function performance(\MyApplication $app, Request $request) {
        return $app->render('common.twig');
    }

    public function generate(\MyApplication $app, Request $request) {
        // 1. Generate hotels and manager accounts, add room types and rooms
        // 2. Generate reservations+roomReservations for the past and fill with stays
        // 3. Generate reservations for the future.
        // 4. Add employees for every hotel

        // For each city
        ob_implicit_flush(true);
        set_time_limit(0);
        $start = microtime(true);
        $prevstart = $start;
        $app['admin_model']->beginTransaction();
        try {
            for ($city = 36020; $city <= 37356; $city++) {
                echo (($city - 36020 + 1)*100.0/(37356-36020))."% , city: " . $city." ";
                echo str_repeat(' ', 4000);
                $cityDate = $app['admin_model']->getStateAndCountryByCity($city);
                $state = $cityDate['stateId'];
                $country = $cityDate['countryId'];
                    $managerLogin = "manager_{$city}_0";
                    $app['user_management']->addUser($managerLogin, '123456', 'ROLE_MANAGER');
                    for ($j = 0; $j < 4; $j++) {
                        $hotelName = $this->generateHotelName();
                        $description = $this->generateHotelDescription();
                        $hotelId = $app['manager_model']->addHotel($hotelName, $country, $state, $city, null, null, 1000, $description);

                        // Adding employees
                        for ($roomNumber = 0; $roomNumber < rand(10, 20); $roomNumber++) {
                            //$name = $this->generateEmployeeName();
                            $salary = rand(200, 3000);
                            $position = $this->generateEmployeePost();
                            $app['manager_model']->addEmployee($hotelId, $this->generateFirstName(),
                                $this->generateLastName(), $this->generateMiddleName(), $position, $salary);
                        }


                        // Generating room types
                        $roomTypeNameCount = rand(4, 10);
                        $roomTypeNames = array();
                        for ($roomNumber = 0; $roomNumber < $roomTypeNameCount; $roomNumber++) {
                            $roomTypeName = $this->generateRoomTypeName();
                            $rtAlreadyExists = true;
                            while($rtAlreadyExists) {
                                $rtAlreadyExists = false;
                                for($p = 0; $p < count($roomTypeNames); $p++) {
                                    if ($roomTypeNames[$p][0] == $roomTypeName) {
                                        $rtAlreadyExists = true;
                                        break;
                                    }
                                }
                                if ($rtAlreadyExists)
                                    $roomTypeName = $this->generateRoomTypeName();
                            }
                            $description = $this->generateRoomTypeDescription();
                            $roomTypeNames[$roomNumber] = array($roomTypeName, 0);
                            $app['manager_model']->addRoomType($hotelId, $roomTypeName, rand(300, 3000), $description);

                        }

                        // Generating rooms
                        $roomCount = rand(100, 300);
                        $roomWithTypes = array();
                        for ($roomNumber = 0; $roomNumber < $roomCount; $roomNumber++) {
                            $number = $roomNumber + 100;
                            $roomTypeIndex = rand(0, count($roomTypeNames) - 1);
                            $roomTypeName = $roomTypeNames[$roomTypeIndex][0];
                            $roomTypeNames[$roomTypeIndex][1]++;
                            $roomWithTypes[$number] = $roomTypeName;
                            $app['manager_model']->addRoom($number, $hotelId, $roomTypeName, 1);
                        }

                        // Generate reservations for all rooms
                        $curDate = new \DateTime(date('Y-m-d'));
                        $date = new \DateTime('2016-10-01');
                        $skip = rand(0, 4);
                        $date->modify('+' . $skip . ' day');
                        $to = new \DateTime('2017-02-01');

                        while ($date < $to) {
                            $duration = rand(3, 10);
                            $checkOutDate = clone $date;
                            $checkOutDate->modify('+' . $duration . ' day');
                            $canceled = rand(1, 5) == 3;    // Probability = 1/5
                            $isPaid = ($date < $curDate && !$canceled) || rand(1, 5) == 3;
                            $email = $this->generateRandomEmail();
                            $phone = rand(1111111111, 9999999999);
                            $reservedRooms = array();

                            for ($t = 0; $t < rand(1, $roomTypeNameCount/2+1); $t++) {

                                $alreadyExists = true;
                                $typeNameIndex = 0;
                                $typeName = $roomTypeNames[$typeNameIndex][0];
                                while ($alreadyExists) {
                                    $typeNameIndex = rand(0, $roomTypeNameCount - 1);
                                    $typeName = $roomTypeNames[$typeNameIndex][0];
                                    $alreadyExists = false;
                                    for ($r = 0; $r < count($reservedRooms); $r++) {
                                        if ($reservedRooms[$r][0] == $typeName) {
                                            $alreadyExists = true;
                                        }
                                    }
                                }
                                $roomCount1 = rand(1, $roomTypeNames[$typeNameIndex][1]);
                                $reservedRooms[$t] = array($typeName, $roomCount1);
                            }

                            $reservationId = $app['search_model']->addReservation(
                                $date->format('Y-m-d'),
                                $checkOutDate->format('Y-m-d'),
                                $hotelId,
                                $email,
                                $phone,
                                $isPaid,
                                $reservedRooms);
                            if ($canceled) {
                                $app['manager_model']->cancelReservation($reservationId);
                            }

                            $alreadyAddedToStay = array(); // contains room numbers
                            // Add stays if not canceled
                            if ($date < $curDate && !$canceled) {
                                for ($u = 0; $u < count($reservedRooms); $u++) {
                                    $amount = $reservedRooms[$u][1];
                                    for ($s = 0; $s < $amount; $s++) {
                                        $roomTypeName = $reservedRooms[$u][0];
                                        $roomNumber = null;
                                        for ($room = 100; $room < $roomCount + 100; $room++) {
                                            if ($roomWithTypes[$room] == $roomTypeName
                                                && !in_array($room, $alreadyAddedToStay)) {
                                                $roomNumber = $room;
                                                array_push($alreadyAddedToStay, $room);
                                                break;
                                            }
                                        }
                                        $firstName = $this->generateFirstName();
                                        $lastName = $this->generateLastName();
                                        $middleName = $this->generateMiddleName();
                                        $app['manager_model']->checkIn($reservationId, $roomNumber, $firstName, $lastName, $middleName);
                                    }
                                }
                            }

                            $skip = rand(1, 4);
                            $date->modify('+' . ($duration + $skip) . ' day');
                        }
                }
                $tmp = microtime(true);
                $time_elapsed_secs = $tmp - $prevstart;
                $prevstart = $tmp;
                echo "Seconds elapsed: $time_elapsed_secs seconds, ".($time_elapsed_secs/60.0)." minutes<br>";

            }
            $app['admin_model']->commit();
        }
        catch(Exception $e) {
            $app['admin_model']->rollback();
            throw $e;
        }
        $time_elapsed_secs = microtime(true) - $start;
        echo "Total seconds elapsed: $time_elapsed_secs seconds, ".($time_elapsed_secs/60.0)." minutes";

        return "\nDone!";
    }

    private function generateHotelName() {
        static $suffix = null;
        if (is_null($suffix)) {
            $rawSuffix = "Hotel Palace Hostel Inn House Plaza Motel";
            $suffix = explode(' ', $rawSuffix);
        }
        static $words = null;
        if (is_null($words)) {
            $rawWords = "Ahwahnee\nAngels\nApple\nFarm\nApple\nValley\nAztec\nBalboa\nBay\nClub & Resort\nBalboa\nBeverly\nWilshire\nBig\nBear\nFrontier\nCal\nNeva\nLodge & Casino\nCarlton\nCarter\nCasa del Desierto\nCasa del Mar\nCecil\nClaremont\nResort\nCloyne\nCourt\nColony\nPalms\nCulver\nDe Anza\nDeetjen's\nBig\nSur\nDisneyland\nGrand\nCalifornian & Spa\nDisney's\nParadise\nPier\nDrakesbad\nGuest\nRanch\nDucey's\nBass\nLake\nLodge\nDunbar\nEast\nBrother\nIsland\nLight\nEl Garces\nEureka\nFairmont\nSan Jose\nGlen\nTavern\nGreen\nShutter\nThe Hacienda\nHacienda\nHot\nSprings\nHayes\nMansion\nHilton\nAnaheim\nHilton\nSan Diego\nBayfront\nHilton\nWaterfront\nBeach\nResort\nHolbrooke\nHollywood\nMelrose\nHollywood\nHoover\nHorton\nGrand\nArcata\nChancellor\nCharlotte\nGroveland\nDel Coronado\nGreen\nLéger\nMac\nMontgomery\nSainte Claire\nWoodland\nThe Keating\nKeyRoute\nLa Costa \nResort and Spa\nLa Quinta\nResort and Club\nLafayette & Suites\nSan Diego\nThe Langham\nHuntington\nMadonna\nManchester\nGrand\nHyatt\nMission\nSan Luis\nObispo\nMurphys\nNational\nExchange\nNew\nCarquinez\nPadre\nPark\nHyatt\nResort\nAviara\nPiedras\nBlancas\nPierpont\nRaymond\nRobinson\nRoy's \nSan Diego\nMarriott\nMarina\nSan Gorgonio\nSanta Barbara\nBiltmore\nShutters on the Beach\nSouthern\nPerris\nSovereign\nStagecoach\nSunset\nTower\nSuperior\nOil\nCompany\nBuilding\nSycamore\nMineral\nSprings\nResort\nThunder\nValley\nCasino\nResort\nU.S. Grant\nVenetian\nCourt\nWagon\nWheel\nWawona\nThe Westin\nSan Diego\nWigwam\nYosemite\nLodge at the Falls\nAmbassador\nClift\nFairmont\nSan Francisco\nFour Seasons\nGrand\nHyatt\nHilton\nFinancial District\nHilton\nUnion Square\nDes Arts\nMajestic\nUnion Square\nHugo\nHuntington\nHyatt\nRegency\nInterContinental\nJW Marriott\nLe Méridien\nMandarin\nOriental\nMark Hopkins\nAntlers Hilton\nAstor\nBeaumont\nThe Broadmoor\nGlenisle\nGrand \nLake\nLodge\nBoulderado\nColorado\nJerome\nKauffman\nOmni \nInterlocken\nResort\nPacific\nRedstone\nSt. Elmo\nThe Stanley\nTeller\nVictor\nWestern\nWestin\nWestminster\nWinks\nPanorama\nBrown\nCourtyard\nDenver\nDowntown\nThe Curtis\nDenver\nMarriott\nCity Center\nFour Seasons\nDenver\nRegency\nDorrance\nElton\nFoxwoods\nResort\nCasino\nThe Griswold\nHampton\nBond\nLight\nMohegan\nSun\nWinvian\nDeer Park\nTavern\nDover\nDowns\nDuPont\nBuilding\nRobinson\nBelleview-Biltmore\nBoca\nRaton\nResort\nCarling\nCasa Marina\nCasa Monica\nChalet Suzanne\nCoral\nGables\nBiltmore\nDixie\nWalesbilt\nDon CeSar\nFort\nHarrison\nGeorge\nWashington\nKenilworth\nLodge\nOrmond\nPolk\nRitz\nRoyal\nPoinciana\nStreamline\nVinoy\nPark\nWell's built\nWorld\nGolf\nVillage";
            $words = explode("\n", $rawWords);
            $words = array_values(array_unique($words));
        }
        $name = '';
        $index = rand(0, count($words)-1);
        $name .= $words[$index].' ';
        $index = rand(0, count($words)-1);
        $name .= $words[$index].' ';
        // Add suffix
        $index = rand(0, count($suffix)-1);
        $name .= $suffix[$index];
        return $name;
    }

    private function generateRoomTypeName() {
        static $adjective = null;
        if (is_null($adjective)) {
            $rawAdjective = "Big Generous Genius Genuine Giving Glamorous Glowing Good Gorgeous Graceful Great Green Grin Growing Fabulous Fair Familiar Famous Fantastic Favorable Fetching Fine Fitting Flourishing Fortunate Free Fresh Friendly adaptable adventurous affable affectionate agreeable ambitious amiable amicable amusing brave bright broad-minded calm careful charming communicative compassionate conscientious considerate convivial courageous courteous creative decisive determined diligent diplomatic discreet dynamic easygoing emotional energetic enthusiastic exuberant fair-minded faithful fearless forceful frank friendly funny generous gentle good gregarious hard-working helpful honest humorous imaginative impartial independent intellectual intelligent intuitive inventive kind loving loyal modest neat nice optimistic passionate patient persistent pioneering philosophical placid plucky polite powerful practical pro-active quick-witted quiet rational reliable resourceful romantic self-confident self-disciplined sensible sensitive shy sincere sociable straightforward sympathetic thoughtful tidy tough unassuming understanding versatile warmhearted willing witty";
            $adjective = explode(' ', $rawAdjective);
            $adjective = array_values(array_unique($adjective));
        }
        static $suffix = null;
        if (is_null($suffix)) {
            $rawSuffix = "King Queen Super Prestige Luxury Room Suite Superior";
            $suffix = explode(' ', $rawSuffix);
        }

        $name = '';
        $index = rand(0, count($adjective)-1);
        $name .= ucfirst($adjective[$index]).' ';

        // Add suffix
        $index = rand(0, count($suffix)-1);
        $name .= ucfirst($suffix[$index]);
        return $name;
    }

    private function generateHotelDescription() {
        $descriptions = array("Offers a spa and wellness centre with massage sessions. They can also order their favourite cocktails at Tambora, Salmarina and the Solarium bars. Beach chairs and towels are available for an extra cost. A business centre is also offered.",
            "This property is a 10-minute walk from the beach. Boasting 3 swimming pools, and a private beach area with bar service, Occidental Cartagena offers rooms with free WiFi and garden views. There are elegant sun terraces with panoramic views.",
            "This property is 2 minutes walk from the beach. This luxurious hotel on the Caribbean beach offers an outdoor swimming pool, elegant colonial architecture and spa facilities. Massage sessions and diving trips can be booked. Wi-Fi is free.",
            "The luxurious rooms at this boutique hotel are totally air-conditioned and come equipped with a mini-bar and a flat-screen TV with cable channels and a DVD player. The spacious bathrooms feature free toiletries. The hotel’s popular restaurant specializes in Mediterranean cuisine, and other options are available just over a quarter mile from the property.",
            "This property is a 12-minute walk from the beach. An outdoor swimming pool, free Wi-Fi and chic décor can be enjoyed at the hotel. The city’s convention center is located across from the hotel.  Air-conditioned rooms may feature garden or city views. All of them include cable TV, minibars and private bathrooms with free toiletries.",
            "This property is 1 minute walk from the beach. Located on the beach, the main tourist and shopping district, this plush hotel offers 4 dining options, a Casino, a pool with sea views and free Wi-Fi. There are event rooms and meeting facilities for business and social use.",
            "This 4-star hotel is located in the city centre, 200 m from the central station. Free Wi-Fi, free parking, a gym, and a 24-hour reception are featured.",
            "Offers a variety of rooms that are clean, air-conditioned, and mostly have panoramic views of the city. All rooms have modern décor, a mini-bar, international TV channels, and enjoy various amenities including bathrobe and slippers.",
            "Every morning a rich buffet breakfast is servied in the on-site restaurant. Guests can also enjoy delicious Tatar and Russian cuisine in the Capella Restaurant, which also serves a range of European dishes. The Shalyapins Piano Bar is open 24-hours.",
            "offers contemporary rooms, studios and suites to cater to everyone’s needs. The modern suites are well-equipped with a seating area and LCD TVs. A business centre and a conference hall are available for business meetings. You can relax in the hot tub and sauna.",
            "It features an indoor swimming pool, massage service and soundproofed rooms with free Wi-Fi. Modern, air-conditioned rooms at the Hayal Hotel include a flat-screen TV with satellite channels and safety deposit box. Some rooms have an electronic lock.",
            "An American-style breakfast buffet and Mediterranean specialties are served in the restaurant. Guests can enjoy international cuisine and exotic cocktails in the Courtyard by Marriott Kazan’s bar, which features a roof terrace with panoramic city views.",
            "Each modern room is colorfully decorated and has wooden touches. The LCD TV is equipped with satellite TV and there is a spacious bathroom. Some rooms have a large seating area. Facilities for disabled guests are also available.",
            "Newly opened at the start of 2009, thsis hotel is a 4-star property located in one of the city's modern buildings. The hotel's 215 rooms are spread over the 17-floors of the tower and feature panoramic views of the city. Rooms are spacious, neatly designed and of course, all rooms have private bathrooms.",
            "The bright, air-conditioned rooms offer a flat-screen TV, a minibar and a private bathroom with heated floors. All rooms have minimalistic, beige and brown interiors. A buffet breakfast is offered every morning. Guests can enjoy lunch and dinner at the on-site restaurant serving European cuisine with chef specialties.",
            "The property features bright room with warm-coloured interiors. Each room comes with a flat-screen TV, wardrobe and fridge. The bathrooms offer a hairdryer and free toiletries. Guests are welcome to visit the on-site café and order various hot dishes, snacks and refreshing drinks. The breakfast is served each morning.",
            "The bright, air-conditioned rooms offer warm-coloured interiors with dark red furnishings and classic-style décor. Each room includes a flat-screen TV, a minibar and a private bathroom with bathrobes, slippers and a hairdryer. The on-site Serbia Restaurant serves European and Kosher cuisine.",
            "You can start each day with a diverse breakfast buffet, served in the elegant restaurant with wooden floors and a piano. In the evenings, international and Russian specialities are offered. Billiards can be enjoyed in the spacious games room, where a well-stocked bar is also available.",
            "Each room includes a flat-screen TV with satellite channels. You will find a kettle in the room. Each room is equipped with a private bathroom. For your comfort, you will find bathrobes, slippers and free toiletries. There is a 24-hour front desk at the property.",
            "Take your breakfast in the elegant guest room. Energize for the day ahead at the large gym. Savour the sauna and solarium, and enjoy a massage. Have a swim in the 25 m pool, and a dip in the spa bath.",
            "With free access to the Health Club, you can have a swim in the pool. Pamper yourself with a massage or beauty treatment, before unwinding with a drink in the cosy bar. The business bar is open 24 hours a day. Here you can meet up for a coffee and a drink or rent a laptop and use the WiFi connection to do some work.",
            "The rooms offer air-conditioning, a flat-screen TV and a fridge. Some rooms come with a seating area. Guests are served a continental breakfast, and the hotel’s restaurant offers European and Georgian cuisine. The snack bar is also available on site."
        );

        $index = rand(0, count($descriptions)-1);
        return $descriptions[$index];
    }

    private function generateRoomTypeDescription() {
        $descriptions = array("Features a king bed, a sitting area, a private bathroom with a shower, LCD TV, DVD player, minibar, coffee machine, safe, and views of the lagoon, garden and pool.",
            "This double room has a sofa and air conditioning.",
            "This double room has air conditioning, minibar and flat-screen TV.",
            "This double room features air conditioning and a sofa.",
            "This quadruple room has a cable TV, sofa and air conditioning.",
            "This suite features a flat-screen TV, sofa and air conditioning.",
            "This family room features air conditioning, microwave and sofa.",
            "Features air conditioning, flat-screen TV, safe, minibar, private balcony or patio, queen size beds, free Wi-Fi and private bathroom with hairdryer.",
            "This double room has cable TV, a minibar and air conditioning.",
            "Features sea views, air conditioning, flat-screen TV, safe, minibar, private balcony or patio, queen size beds, free Wi-Fi and private bathroom with hairdryer.",
            "The suite features a king size bed, a living room with a bar, refrigerator and microwave, swimming pool views, private bathroom with spa bathtub, and free Wi-Fi.",
            "Features air conditioning, cable TV, minibar and private bathroom.",
            "This apartment features a king bed, a sofa bed, a dining area, kitchen, cable TV, minibar, private bathroom with hot water, desk, telephone, safe and digital locks.",
            "This air-conditioned suite features the largest space and TV.",
            "Room with 3 single beds or one double and one single. It features A/C, free Wi-Fi, cable TV and minibar.",
            "This room features a single bed, air conditioning, cable TV, free Wi-Fi and a private bathroom. Some rooms may feature views of Cartagena Bay, San Felipe Castle or the Caribbean Sea and the city's historic center.",
            "This apartment has a tile/marble floor, stovetop and kitchenware.",
            "This double room has a balcony, air conditioning and a minibar.",
            "Offering excellent views of the Caribbean Sea, this room features a spacious seating area and a flat-screen TV with satellite channels. Guests also enjoy a private terrace, a minibar and a wardrobe.",
            "This room is air-conditioned and features a safety-deposit box, a flat-screen satellite TV and a private bathroom with a shower and free toiletries.",
            "This spacious air-conditioned suite has an ocean view and a game table with Backgammon, Chess and Checkers.",
            "On levels 6 to 11, this room offers views of Beverly Hills from the private balcony. It features a 42-inch flat-screen TV, an iPod dock and alarm clock, and a marble bathroom. ",
            "On floors 10 and 11, this suite offers city views, and a separate living area. Floor to ceiling sliding door windows separate the suite from the balcony.",
            "Featuring views of the city, this suite is on floors 10 and 11. A floor-to-ceiling window sliding door opens out to a private balcony. A separate living room is included."
    );

        $index = rand(0, count($descriptions)-1);
        return $descriptions[$index];
    }

    private function generateFirstName() {
        static $names = null;
        if (is_null($names)) {
            $rawNames = "Mohamed Abdelkader Ahmed Mohammed Ali Rachid Said Brahim Omar Djamel Fatima Sara Fatiha Aicha Fatma Amina Meriem Karima Kheira Nadia Santiago Mateo Juan Matías Nicolás Benjamín Pedro Tomás Thiago Santino Sofía María Lucía Martina Catalina Elena Emilia Valentina Paula Zoe William James John Mason Elijah Noah Jackson Carter Michael Samuel Liam Noah James William Oliver Joseph Owen Gabriel Alexander Michael Wyatt Jackson Jacob Benjamin Logan Samuel Noah Liam Alexander Michael Ethan Daniel Jacob Sebastian Aiden Julian William Elijah Mason Noah James Liam Jacob John Gabriel Carter Noah Jacob Ethan Daniel Matthew Alexander Jayden Sebastian Liam David Liam William Jackson Alexander Oliver Benjamin Noah James Wyatt Logan Noah Mason Alexander Liam Benjamin Jacob William Michael Logan Matthew Joseph Mason Noah Liam Jackson Michael Alexander Benjamin Anthony Luke William Aiden James Christopher William Henry James Alexander Jacob Michael Noah Samuel Benjamin Charles Daniel Dylan John Matthew David Liam Noah Jacob Mason Ethan Lucas Alexander Michael Elijah Daniel Matthew William Noah Mason Elijah James Aiden Jacob Jackson Carter Ethan Liam Noah Liam Mason Ezekiel Aiden William Elijah Ethan Logan Caleb Luke Ezra Alexander Liam William Oliver James Wyatt Logan Elijah Lucas Jackson Noah Carter Hudson Mason Noah Liam Alexander Jacob William Michael Benjamin Daniel Mason James Liam Noah Oliver Elijah Mason William Jackson Carter Lucas Owen Liam William Oliver Henry Owen Noah Mason Elijah Wyatt Jackson Liam William Mason Oliver Wyatt Noah Ethan Henry Owen Benjamin Carter Logan Alexander William Mason Elijah James Noah Jackson Liam Hunter Jaxon Levi Aiden John Noah Mason Liam Elijah William Carter John Aiden James Luke Liam Owen Mason Oliver Wyatt Jackson William Hunter Lucas Noah Alexander Benjamin Carter Henry Noah Logan James Mason Jacob William Liam Michael Ethan Benjamin Benjamin William Noah Mason James Liam Jacob Lucas Michael Jack Logan Noah Liam Carter Mason Lucas Jacob Benjamin Jackson Owen Elijah Logan Henry Oliver William Liam Mason Jack Owen Jackson Lucas James Ethan William John James Mason Elijah Aiden Noah Carter Jayden Christian Liam William Mason Henry Jackson Oliver Elijah Noah Carter James William Liam Wyatt Oliver James Carter Mason Henry Hunter Logan Luke Noah Samuel Michael Jackson Henry William Liam Noah Oliver Owen Carter Samuel Mason Isaac Noah Liam Alexander Jacob Anthony Michael Jayden Sebastian Daniel Julian Mason Ethan Jackson Benjamin Owen Mason Noah Logan William Jack Liam Oliver Carter Alexander Liam Michael Jacob Noah Mason Matthew Joseph Dylan Anthony Daniel Noah Elijah Liam Michael Aiden Josiah Alexander Gabriel Jacob Daniel Matthew Liam Jacob Ethan Noah Michael Matthew Mason Joseph Lucas Daniel William Noah Mason Liam James Elijah Jackson Jacob Carter Aiden Liam Oliver Carter Owen Henry Mason William Noah Easton Wyatt Hudson Jaxon Lucas Lincoln Elijah Liam Mason Noah Carter William Jackson Logan Owen Benjamin James Henry Jacob Elijah Jaxon William Liam Noah Wyatt Mason James Jackson Benjamin Gabriel Liam Henry Oliver James Noah Mason Wyatt Elijah Alexander William Benjamin Jackson Logan Mason Liam Noah Michael Jacob Logan Benjamin Carter James Lucas Noah Liam Mason Logan Alexander Michael Benjamin Lucas Jacob Joseph William James Mason Noah Elijah Liam Michael Jackson John Aiden Carter Oliver Owen Liam Jackson Levi Mason Henry James Benjamin Hudson Noah Hunter Jacob William James Elijah Mason Noah Jackson Liam John Jacob Carter Aiden Noah Liam Jacob Ethan Daniel Matthew Sebastian Alexander Jayden Jose Aiden William Oliver James Liam Lincoln Henry Mason Jack Owen Benjamin Liam Oliver Jackson Mason Owen Carter Wyatt Samuel Noah Alexander Logan William Noah Liam James Mason Jackson Jacob Elijah Michael Benjamin Oliver Noah Liam Benjamin Henry William Logan Alexander Samuel James Mason Noah Liam Mason Hunter Levi Wyatt James Jaxon William Easton Oliver Owen Liam Noah Mason Henry Jackson Logan William Benjamin Liam Mason Benjamin Wyatt Luke Noah Logan Jaxon Ryker Carter Isaac Samuel Jace Oliver Alexander Henry";
            $names = array_values(array_unique(explode(' ', $rawNames)));
        }
        $index = rand(0, count($names)-1);
        return ucfirst($names[$index]);
    }

    private function generateLastName() {
        static $surNames = null;
        if (is_null($surNames)) {
            $rawSurNames = "Smirnov Ivanov Kuznetsov Popov Sokolov Lebedev Kozlov Novikov Morozov Petrov Volkov Solovyov Vasilyev Zaytsev Pavlov Semyonov Golubev Vinogradov Bogdanov Vorobyov Smith Jones Taylor Brown Williams Wilson Johnson Davies Robinson Wright Thompson Evans Walker White Roberts Green Hall Wood Jackson Clarke Brown Smith Patel Jones Williams Johnson Taylor Thomas Roberts Khan Lewis Jackson Clarke James Phillips Wilson Ali Mason Mitchell Rose Davis Davies Rodríguez Cox Alexander González Rodríguez Hernández Pérez García Martín Santana Díaz Suárez Sánchez López Cabrera Ramos Medina Fernández Morales Delgado Marrero León Alonso Herrera Cruz Domínguez Gutiérrez Reyes Torres Alvarez Rivero Armas Trujillo";
            $surNames = array_values(array_unique(explode(' ', $rawSurNames)));
        }
        $index = rand(0, count($surNames)-1);
        return ucfirst($surNames[$index]);
    }

    private function generateMiddleName() {
        static $middleNames = null;
        if (is_null($middleNames)) {
            $rawMiddleNames = "Aaron Aiden Anton Aubrey Avery Bailey Bennett Blair Braiden Brendon Brett Brian Bryce Byron Carson Chance Charles Chase Claude Clinton Colten Conrad Craig Cullen Damon Dante Dawson Dayton Denver Denzel Dillon Drake Dwayne Edwin Ellis Emmett Ethan Francis Garrett Glenn Grant Holden Houston Hyrum Ivan Jace Jackson Jade Jarrett Juan Justin Keaton Kelton Layne Layton Lincoln Louis Lyle Malcolm Marshall Mitchell Myron Noah Noel Owen Payton Peyton Preston Quinn Quintin Randall Reese Rhett Riley River Rory Ryder Sawyer Sean Seth Shane Tilton Shelton Stewart Tanner Taylor Thomas Trevor Trenton Tristan Tyrone Tyson Vernon Wade Warren Wesley Weston Wilson Winston Zachariah Aleksandrovich Aleksandrovna Alekseyevich Alekseyevna Anatolyevich Anatolyevna Andreyevich Andreyevna Antonovich Antonovna Arkadyevich Arkadyevna or: Arkadiyevich Arkadiyevna Arsenyevich Arsenyevna Artemovich Artemovna Artemyevich Artemyevna Arturovich Arturovna Borisovich Borisovna Chingizovich Chingizovna Denisovich Denisovna Dmitriyevich Dmitriyevna Danilovich Danilovna Eduardovich Eduardovna Ernestovich Ernestovna Fedorovich Fedorovna Feliksovich Feliksovna Filippovich Filippovna Gennadyevich Gennadyevna or: Gennadiyevich Gennadiyevna Georgiyevich Georgiyevna Germanovich Germanovna Glebovich Glebovna Grigoryevich Grigoryevna Guryevich Guryevna Ibragimovich Ibragimovna Igorevich Igorevna Illarionovich Illarionovna Ilyich Ilyinichna Innokentyevich Innokentyevna Iosifovich Iosifovna Ivanovich Ivanovna Karpovich Karpovna Kazimirovich Kazimorovna Kirillovich Kirillovna Klementyevich Klementyevna Konstantinovich Konstantinovna Kuzmich Kuzminichna Leonidovich Leonidovna Leontyevich Leontyevna Leybovich Leybovna Lukich Lukinichna Lvovich Lvovna Maksimovich Maksimovna Maratovich Maratovna Matveyevich Matveyevna Mikhaylovich Mikhaylovna Mironovich Mironovna Moiseyevich Moiseyevna Nikitich Nikitichna Nikonorovich Nikonorovna Nikolayevich Nikolayevna Olegovich Olegovna Orestovich Orestovna Pavlovich Pavlovna Petrovich Petrovna Pimenovich Pimenovna Platonovich Platonovna Rashidovich Rashidovna Rodionovich Rodionovna Rostislavovich Rostislavovna Ruslanovich Ruslanovna Rustamovich Rustamovna Ryurikovich Ryurikovna Safronovich Safronovna Savelyevich Savelyevna Semenovich Semenovna Serafimovich Serafimovna Sergeyevich Sergeyevna Stanislavovich Stanislavovna Timofeyevich Timofeyevna Timurovich Timurovna Ulyanovich Ulyanovna Vasilyevich Vasilyevna Valentinovich Valentinovna Valeryevich Valeryevna or: Valeriyevich Valeriyevna Veniaminovich Veniaminovna Viktorovich Viktorovna Vitalyevich Vitalyevna or: Vitaliyevich Vitaliyevna Vladimirovich Vladimirovna Vladislavovich Vladislavovna Vsevolodovich Vsevolodovna Vyacheslavovich Vyacheslavovna Yegorovich Yegorovna Yemelyanovich Yemelyanovna Yevgenyevich Yevgenyevna Yulyevich Yulyevna Yuryevich Yuryevna Zakharovich Zakharovna Zinovyevich Zinovyevna Zhoresovich Zhoresovna";
            $middleNames = array_values(array_unique(explode(' ', $rawMiddleNames)));
        }
        $index = rand(0, count($middleNames)-1);
        return ucfirst($middleNames[$index]);
    }

    private function generateEmployeePost() {
        static $posts = null;
        if (is_null($posts)) {
            $rawPosts = "Director\nCleaning staff\nWorker\nRepairer\nReceptionist\nManager\nAdministrator\nCook\nChef";
            $posts = array_values(array_unique(explode("\n", $rawPosts)));
        }
        $post = '';
        $index = rand(0, count($posts)-1);
        $post .= ucfirst($posts[$index]);
        return $post;
    }

    // Credits: http://www.jonhaworth.com/articles/php/generate-random-email-addresses
    private function generateRandomEmail() {
        $tlds = array("com", "net", "gov", "org", "edu", "biz", "info");
        $char = "0123456789abcdefghijklmnopqrstuvwxyz";
        $ulen = mt_rand(5, 10);
        $dlen = mt_rand(7, 17);
        $a = "";
        for ($i = 1; $i <= $ulen; $i++) {
            $a .= substr($char, mt_rand(0, strlen($char)), 1);
        }
        $a .= "@";

        for ($i = 1; $i <= $dlen; $i++) {
            $a .= substr($char, mt_rand(0, strlen($char)), 1);
        }

        $a .= ".";
        $a .= $tlds[mt_rand(0, (sizeof($tlds) - 1))];
        return $a;
    }
}