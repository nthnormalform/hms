<?php
namespace app\Controller;

use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class ManagerController extends BaseController implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app["controllers_factory"];
        $controllers->get("/", "app\\Controller\\ManagerController::index")
            ->bind('manager');
        $controllers->get("/hotels", "app\\Controller\\ManagerController::hotels")
            ->bind('manage_hotels');
        $controllers->post("/hotels", "app\\Controller\\ManagerController::addHotel")
            ->bind('manage_add_hotel');
        $controllers->put("/hotels", "app\\Controller\\ManagerController::editHotel")
            ->bind('manage_edit_hotel');
        $controllers->delete("/hotels", "app\\Controller\\ManagerController::deleteHotel")
            ->bind('manage_delete_hotel');

        $controllers->get("/reservations", "app\\Controller\\ManagerController::reservations")
            ->bind('manage_reservations');

        $controllers->get("/roomtypes", "app\\Controller\\ManagerController::roomtypes")
            ->bind('manage_roomtypes');
        $controllers->post("/roomtypes", "app\\Controller\\ManagerController::addRoomType")
            ->bind('manage_add_roomtype');
        $controllers->put("/roomtypes", "app\\Controller\\ManagerController::editRoomType")
            ->bind('manage_edit_roomtype');
        $controllers->delete("/roomtypes", "app\\Controller\\ManagerController::deleteRoomType")
            ->bind('manage_delete_roomtype');

        $controllers->get("/rooms", "app\\Controller\\ManagerController::rooms")
            ->bind('manage_rooms');
        $controllers->post("/rooms", "app\\Controller\\ManagerController::addRoom")
            ->bind('manage_add_room');
        $controllers->put("/rooms", "app\\Controller\\ManagerController::editRoom")
            ->bind('manage_edit_room');
        $controllers->delete("/rooms", "app\\Controller\\ManagerController::deleteRoom")
            ->bind('manage_delete_room');

        $controllers->get("/accounts", "app\\Controller\\ManagerController::accounts")
            ->bind('manage_accounts');
        $controllers->put("/accounts", "app\\Controller\\ManagerController::changePassword")
            ->bind('manage_change_password');

        $controllers->get("/employees", "app\\Controller\\ManagerController::employees")
            ->bind('manage_employees');
        $controllers->post("/employees", "app\\Controller\\ManagerController::addEmployee")
            ->bind('manage_add_employee');
        $controllers->put("/employees", "app\\Controller\\ManagerController::editEmployee")
            ->bind('manage_edit_employee');
        $controllers->delete("/employees", "app\\Controller\\ManagerController::deleteEmployee")
            ->bind('manage_delete_employee');

        $controllers->get("/change", "app\\Controller\\ManagerController::changeHotel")
            ->bind('change_hotel');

        return $controllers;
    }

    /**
     * Manager can manage multiple hotels. We will store current hotelID in user session.
     * Current hotelID can't be null. If it's null, set it to the first managed hotel id.
     *
     * @param \MyApplication $app
     * @return mixed
     */
    private function getCurrentHotelId(\MyApplication $app) {
        $this->checkRole($app, 'ROLE_MANAGER');
        $hotelId = $app['session']->get('hotelid');
        if (is_null($hotelId)) {
            // Get the first hotelId among all managed hotels
            $hotelId = $app['manager_model']->getRandomManagedHotel();
            if(is_null($hotelId))
                $hotelId = -1;
            $app['session']->set('hotelid', $hotelId);
        }
        return $hotelId;
    }

    private function setCurrentHotelId(\MyApplication $app, $hotelId) {
        $app['session']->set('hotelid', $hotelId);
    }

    public function index(\MyApplication $app, Request $request) {
        // Get current hotel id
        $hotelId = $this->getCurrentHotelId($app);
        $hotels = $app['manager_model']->getManagedHotels();
        $occupiedRooms = $app['manager_model']->getAllRooms($hotelId);
        $statuses = array(0=>'Occupied',
            1=>'Unoccupied',
            2=>'Dirty',
            3=>'Cleaning',
            4=>'Unavailable',
            5=>'Need maintenance',
            6=>'Under maintenance');
        $roomStatuses = $app['manager_model']->getRoomStatuses($hotelId);

        $newRoomStatuses = array();

        for ($i = 0; $i <= 6; $i++) {
            $found = FALSE;
            for ($j = 0; $j < count($roomStatuses); $j++) {
                if (!is_null($roomStatuses[$j]) && $roomStatuses[$j]['status'] == $i) {
                    $newRoomStatuses[$i] = $roomStatuses[$j]['count'];
                    $found = TRUE;
                    break;
                }
            }
            if (!$found) $newRoomStatuses[$i] = '0';
        }

        return $app->render('manage.twig', array('managedhotels'=>$hotels, 'hotelid'=>$hotelId, 'rooms'=>$occupiedRooms,
            'statuses'=>$statuses, 'roomstatuses'=>$newRoomStatuses));
    }

    public function changeHotel(\MyApplication $app, Request $request) {
        $this->checkRole($app, 'ROLE_MANAGER');
        $hotelId = $request->get('id');

        if (is_null($hotelId)) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('manage_hotels'));
        }
        $this->setCurrentHotelId($app, $hotelId);

        return $app->redirect($request->headers->get('referer'));
    }

    /*
     * Hotels
     */
    public function hotels(\MyApplication $app, Request $request) {
        $hotels = $app['manager_model']->getManagedHotels();
        $destinations = $app['search_model']->getDestinationsAsString();
        $managedHotels = $app['manager_model']->getManagedHotels();
        $hotelId = $this->getCurrentHotelId($app);

        return $app->render('hotels.twig', array('hotels'=>$hotels, 'destinations'=>$destinations, 'managedhotels'=>$managedHotels, 'hotelid'=>$hotelId));
    }

    public function addHotel(\MyApplication $app, Request $request) {
        $name = $request->get('name');
        $country = $request->get('countryid');
        $state = $request->get('stateid');
        $city = $request->get('cityid');
        $longitude = $request->get('longitude');
        $latitude = $request->get('latitude');
        $budget = $request->get('budget');
        $description = $request->get('description');

        if ($this->bs(array($name, $description))) {
            $app->flashError("Please enter name and description!");
            return $app->redirect($app->path('manage_hotels'));
        }

        if ($this->bn(array($country, $state, $city))) {
            $app->flashError("Please enter correct country, state, city!");
            return $app->redirect($app->path('manage_hotels'));
        }

        if ($longitude != '' && ($this->bn(array($longitude)) || $longitude < -180 || $longitude > 180)) {
            $app->flashError("Please enter correct longitude!");
            return $app->redirect($app->path('manage_hotels'));
        }

        if ($latitude != '' && ($this->bn(array($latitude)) || $latitude < -90 || $latitude > 90)) {
            $app->flashError("Please enter correct latitude!");
            return $app->redirect($app->path('manage_hotels'));
        }

        $app['manager_model']->addHotel($name, $country, $state, $city, $longitude, $latitude, $budget, $description);
        $app->flashSuccess("Hotel was successfully added.");

        return $app->redirect($app->path('manage_hotels'));
    }

    public function editHotel(\MyApplication $app, Request $request) {
        $hotelID = $request->get('id');
        $name = $request->get('name');
        $country = $request->get('countryid');
        $state = $request->get('stateid');
        $city = $request->get('cityid');
        $longitude = $request->get('longitude');
        $latitude = $request->get('latitude');
        $budget = $request->get('budget');
        $description = $request->get('description');

        if ($this->bn(array($hotelID))) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('manage_hotels'));
        }

        if ($this->bs(array($name, $description))) {
            $app->flashError("Please enter name and description!");
            return $app->redirect($app->path('manage_hotels'));
        }

        if ($this->bn(array($country, $state, $city))) {
            $app->flashError("Please enter correct country, state, city!");
            return $app->redirect($app->path('manage_hotels'));
        }

        if ($longitude != '' && ($this->bn(array($longitude)) || $longitude < -180 || $longitude > 180)) {
            $app->flashError("Please enter correct longitude!");
            return $app->redirect($app->path('manage_hotels'));
        }

        if ($latitude != '' && ($this->bn(array($latitude)) || $latitude < -90 || $latitude > 90)) {
            $app->flashError("Please enter correct latitude!");
            return $app->redirect($app->path('manage_hotels'));
        }

        $app['manager_model']->editHotel($hotelID, $name, $country, $state, $city, $longitude, $latitude, $budget, $description);
        $app->flashSuccess("Hotel was successfully edited.");

        return $app->redirect($app->path('manage_hotels'));
    }

    public function deleteHotel(\MyApplication $app, Request $request) {
        $id = $request->get('id');
        if ($this->bn(array($id))) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('manage_hotels'));
        }

        $app['manager_model']->removeHotel($id);
        $app->flashSuccess("Hotel was successfully deleted.");
        return $app->redirect($app->path('manage_hotels'));
    }

    /*
     * Roomtypes
     */
    public function roomtypes(\MyApplication $app, Request $request) {
        $managedHotels = $app['manager_model']->getManagedHotels();
        $hotelId = $this->getCurrentHotelId($app);
        $roomTypes = $app['manager_model']->getRoomTypes($hotelId);

        return $app->render('roomtypes.twig', array('managedhotels'=>$managedHotels, 'hotelid'=>$hotelId, 'roomtypes'=>$roomTypes));
    }

    public function addRoomType(\MyApplication $app, Request $request) {
        $hotelID = $this->getCurrentHotelId($app);
        $roomTypeName = $request->get('name');
        $price = $request->get('price');
        $description = $request->get('description');

        if ($this->bn(array($hotelID))) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('manage_roomtypes'));
        }

        if ($this->bn(array($price)) || $price < 0) {
            $app->flashError("Please enter valid price!");
            return $app->redirect($app->path('manage_roomtypes'));
        }

        if ($this->bs(array($roomTypeName, $description))) {
            $app->flashError("Please enter valid name and description!");
            return $app->redirect($app->path('manage_roomtypes'));
        }

        $app['manager_model']->addRoomType($hotelID, $roomTypeName, $price, $description);
        $app->flashSuccess("Room type was successfully added.");

        return $app->redirect($app->path('manage_roomtypes'));
    }

    public function editRoomType(\MyApplication $app, Request $request) {
        $hotelID = $this->getCurrentHotelId($app);
        $newRoomTypeName = $request->get('name');
        $oldRoomTypeName = $request->get('oldname');
        $price = $request->get('price');
        $description = $request->get('description');

        if ($this->bn(array($hotelID))) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('manage_roomtypes'));
        }

        if ($this->bn(array($price)) || $price < 0) {
            $app->flashError("Please enter valid price!");
            return $app->redirect($app->path('manage_roomtypes'));
        }

        if ($this->bs(array($oldRoomTypeName, $newRoomTypeName, $description))) {
            $app->flashError("Please enter valid names and description!");
            return $app->redirect($app->path('manage_roomtypes'));
        }

        $app['manager_model']->editRoomType($hotelID, $oldRoomTypeName, $newRoomTypeName, $price, $description);
        $app->flashSuccess("Room type was successfully edited.");

        return $app->redirect($app->path('manage_roomtypes'));
    }

    public function deleteRoomType(\MyApplication $app, Request $request) {
        $hotelID = $this->getCurrentHotelId($app);
        $roomTypeName = $request->get('name');

        if ($this->bn(array($hotelID))) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('manage_roomtypes'));
        }

        if ($this->bs(array($roomTypeName))) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('manage_roomtypes'));
        }

        $app['manager_model']->removeRoomType($hotelID, $roomTypeName);
        $app->flashSuccess("Room type was successfully deleted.");

        return $app->redirect($app->path('manage_roomtypes'));
    }

    /*
     * Rooms
     */
    public function rooms(\MyApplication $app, Request $request) {
        $managedHotels = $app['manager_model']->getManagedHotels();
        $hotelId = $this->getCurrentHotelId($app);
        $hotelRooms = $app['manager_model']->getRooms($hotelId);
        $roomTypes = $app['manager_model']->getRoomTypes($hotelId);
        $statuses = array(0=>'Occupied',
                        1=>'Unoccupied',
                        2=>'Dirty',
                        3=>'Cleaning',
                        4=>'Unavailable',
                        5=>'Need maintenance',
                        6=>'Under maintenance');

        return $app->render('rooms.twig', array('managedhotels'=>$managedHotels, 'hotelid'=>$hotelId,
            'rooms'=>$hotelRooms, 'roomtypes'=>$roomTypes, 'statuses'=>$statuses));
    }

    public function addRoom(\MyApplication $app, Request $request) {
        $hotelID = $this->getCurrentHotelId($app);
        $roomNumber = $request->get('number');
        $roomTypeName = $request->get('type');
        $status = $request->get('status');

        if ($this->bn(array($hotelID))) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('manage_rooms'));
        }

        if ($this->bn(array($roomNumber, $status)) || $roomNumber < 0 || $status < 0) {
            $app->flashError("Please enter valid room number and status!");
            return $app->redirect($app->path('manage_rooms'));
        }

        if ($this->bs(array($roomTypeName))) {
            $app->flashError("Please enter valid name!");
            return $app->redirect($app->path('manage_rooms'));
        }

        $app['manager_model']->addRoom($roomNumber, $hotelID, $roomTypeName, $status);
        $app->flashSuccess("Room was successfully added.");

        return $app->redirect($app->path('manage_rooms'));
    }

    public function editRoom(\MyApplication $app, Request $request) {
        $hotelID = $this->getCurrentHotelId($app);
        $newRoomNumber = $request->get('number');
        $oldRoomNumber = $request->get('oldnumber');
        $roomTypeName = $request->get('type');
        $status = $request->get('status');

        if ($this->bn(array($hotelID))) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('manage_rooms'));
        }

        if ($this->bn(array($newRoomNumber, $oldRoomNumber, $status)) || $newRoomNumber < 0 || $oldRoomNumber < 0 || $status < 0) {
            $app->flashError("Please enter valid room number and status!");
            return $app->redirect($app->path('manage_rooms'));
        }

        if ($this->bs(array($roomTypeName))) {
            $app->flashError("Please enter valid name!");
            return $app->redirect($app->path('manage_rooms'));
        }

        $app['manager_model']->editRoom($hotelID, $oldRoomNumber, $newRoomNumber, $roomTypeName, $status);
        $app->flashSuccess("Room was successfully edited.");

        return $app->redirect($app->path('manage_rooms'));
    }

    public function deleteRoom(\MyApplication $app, Request $request) {
        $hotelID = $this->getCurrentHotelId($app);
        $roomNumber = $request->get('number');

        if ($this->bn(array($hotelID, $roomNumber))) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('manage_rooms'));
        }

        $app['manager_model']->removeRoom($hotelID, $roomNumber);
        $app->flashSuccess("Room was successfully deleted.");

        return $app->redirect($app->path('manage_rooms'));
    }

    /*
     * Accounts
     */
    public function accounts(\MyApplication $app, Request $request) {
        $managedHotels = $app['manager_model']->getManagedHotels();
        $hotelId = $this->getCurrentHotelId($app);
        $accounts = $app['manager_model']->getAccounts($hotelId);

        return $app->render('accounts.twig', array('managedhotels'=>$managedHotels, 'hotelid'=>$hotelId, 'accounts'=>$accounts));
    }

    public function changePassword(\MyApplication $app, Request $request) {
        $login = $request->get('login');
        $password = $request->get('password');
        $password2 = $request->get('password2');

        if ($this->bs(array($login))) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('manage_accounts'));
        }

        if ($this->bs(array($password, $password2))) {
            $app->flashError("Please an a valid password!");
            return $app->redirect($app->path('manage_accounts'));
        }

        if ($password2 != $password) {
            $app->flashError('Entered passwords don\'t match!');
            return $app->redirect($app->path('manage_accounts'));
        }

        $app['user_management']->changePassword($login, $password);
        $app->flashSuccess("Password was successfully changed. ");

        return $app->redirect($app->path('manage_accounts'));
    }

    /*
     * Employees
     */
    public function employees(\MyApplication $app, Request $request) {
        $hotelId = $this->getCurrentHotelId($app);
        $managedHotels = $app['manager_model']->getManagedHotels();
        if (is_null($hotelId)) {
            $app->flashError("Internal error :( !");
            return $app->redirect($app->path('manage_employees'));
        }

        $employees = $app['manager_model']->getEmployees($hotelId);
        return $app->render('employees.twig', array('employees'=>$employees, 'managedhotels'=>$managedHotels, 'hotelid'=>$hotelId));
    }

    public function addEmployee(\MyApplication $app, Request $request) {
        $firstName = $request->get('firstname');
        $lastName = $request->get('lastname');
        $middleName = $request->get('middlename'); //  allow it to be empty or null
        $salary = $request->get('salary');
        $post = $request->get('post');
        if ($this->bs(array($firstName, $lastName, $post))) {
            $app->flashError("Please enter first name, last name and a post!");
            return $app->redirect($app->path('manage_employees'));
        }

        if ($this->bn(array($salary)) && $salary <= 0) {
            $app->flashError("Please enter a positive salary!");
            return $app->redirect($app->path('manage_employees'));
        }

        $hotelId = $this->getCurrentHotelId($app);
        if (is_null($hotelId)) {
            $app->flashError("Internal error :( !");
            return $app->redirect($app->path('manage_employees'));
        }
        $app['manager_model']->addEmployee($hotelId, $firstName, $lastName, $middleName, $post, $salary);
        $app->flashSuccess("Employee was successfully added.");

        return $app->redirect($app->path('manage_employees'));
    }

    public function editEmployee(\MyApplication $app, Request $request) {
        $firstName = $request->get('firstname');
        $lastName = $request->get('lastname');
        $middleName = $request->get('middlename'); //  allow it to be empty or null
        $id = $request->get('id');
        $salary = $request->get('salary');
        $post = $request->get('post');
        if ($this->bs(array($firstName, $lastName, $post))) {
            $app->flashError("Please enter first name, last name and a post!");
            return $app->redirect($app->path('manage_employees'));
        }

        if ($this->bn(array($salary, $id)) && $salary <= 0) {
            $app->flashError("Please enter a positive salary!");
            return $app->redirect($app->path('manage_employees'));
        }

        $app['manager_model']->editEmployee($id, $firstName, $lastName, $middleName, $post, $salary);
        $app->flashSuccess("Employee was successfully edited.");

        return $app->redirect($app->path('manage_employees'));
    }

    public function deleteEmployee(\MyApplication $app, Request $request) {
        $id = $request->get('id');
        if ($this->bn(array($id))) {
            $app->flashError("Internal error :(");
            return $app->redirect($app->path('manage_employees'));
        }

        $app['manager_model']->removeEmployee($id);
        $app->flashSuccess("Employee was successfully deleted.");
        return $app->redirect($app->path('manage_employees'));
    }

    /*
     * Reservations
     */
    public function reservations(\MyApplication $app, Request $request) {
        $hotelId = $this->getCurrentHotelId($app);
        $reservations = $app['manager_model']->getReservationsFromToday($hotelId);
        $roomTypes = $app['manager_model']->getAllRoomTypes($hotelId);

        $roomReservations = $app['manager_model']->getAllRoomReservationsByRID($hotelId);

        return $app->render('reception_reservations.twig',
            array('reservations' => $reservations, 'roomtypes'=>$roomTypes, 'roomReservations'=>$roomReservations, 'managermode'=>true));
    }
}