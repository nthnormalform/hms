# Installation

1. Clone this repository:
```
mkdir /path/to/your/project
cd /path/to/your/project
git init
git remote add origin https://igevorse@bitbucket.org/nthnormalform/hms.git
```

2. Install Composer - a dependency Manager for PHP: [ Get composer! ](https://getcomposer.org/)

3. Run `composer update` to download all vendor files.

4. Set up a local server to quickly access the website.

5. Make a virtual host, for example 'hms.dev' if you want: [ See tutorial ](https://habrahabr.ru/sandbox/80431/).

6. Install a local MySQL server.

7. Use `app/setup/setup.sql` to create MySQL user and database.

8. Import `app/setup/data.sql` to fill MySQL tables with data.

9. Open your virtual host in browser and use the web application!

# Git workflow

1. Create a new branch for your new feature.

2. Make some commits.

3. Get the latest changes from the server: `git fetch`

4. Rebase on master.
```
git checkout your-branch-name
git rebase master
```

5. Push your branch to a server: `git push -u origin your-branch-name`

6. Let your teammate review the code and then merge to master.